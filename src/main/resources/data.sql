INSERT INTO `question_category` (`id`, `name`)
VALUES (1, 'sport'),
       (2, 'animals'),
       (3, 'movie'),
       (4, 'celebrity'),
       (5, 'geography'),
       (6, 'history'),
       (7, 'other'),
       (8, 'android');

INSERT
INTO `question_type` (`id`, `name`)
VALUES (1, 'QCM'),
       (2, 'Question'),
       (3, 'Petit bac');


INSERT INTO `avatar` (`id`, `image`)
VALUES (1,
        'https://images.sudouest.fr/2020/04/06/5e8b446366a4bdca324114fe/widescreen/1200x600/jean-castex-connait.jpg?v1'),
       (2, 'https://m105.ca/wp-content/uploads/2020/11/123511587_464824704483038_1486874161085510814_n-2.png');


INSERT INTO `media` (`id`, `source`)
VALUES (1, 'https://data.whicdn.com/images/220615184/original.gif'),
       (2,
        'https://upload.wikimedia.org/wikipedia/commons/6/66/G%C3%A9rald_Darmanin_2019_%28cropped%29.jpg'),
       (4, 'https://images.frandroid.com/wp-content/uploads/2017/02/bugdroid.png');


INSERT INTO `quiz` (`id`, `description`, `lang`, `name`, `rate`)
VALUES (1, 'Ce quiz regroupe plusieurs questions', 'fr', 'Quiz1', NULL),
       (3, 'The quiz 2 is lying', 'en', 'Quiz3', 2.0),
       (4, 'And the quiz 3 are you better ?', 'en ', 'Quiz4', 2.2),
       (5, 'The quiz is not a question the Quiz4', 'en', 'Quiz5', 1.7),
       (6, 'Ce quiz est meilleur que l''autre quiz français', 'fr', 'Quiz6', 2.4),
       (7, 'Arrêtez de vous batter les quiz', 'fr', 'Quiz7', 3.3),
       (8, 'Ningen wa mondai desu', 'jp', 'Mondai1', 4.0),
       (9, 'Je pense qu''on vit dans un monde libre et évolutive donc j''ai raison pour le titre', 'fr', 'Quizz', 4.1),
       (10, 'The ultimate quiz', 'en', 'Ultimat quiz', 5.0),
       (11, 'En faite un quiz c''est des questions', 'fr', 'Questions', NULL),
       (12, 'Quiz sur les bases d''Android', 'fr', 'Base d''Android', NULL),
       (21, 'Quiz test vote', 'en', 'test vote', NULL);

INSERT INTO `question` (`id`, `good_answer`, `lang`, `points`, `position`, `time`, `wording`, `media_id`, `category_id`,
                        `type_id`, `quiz_id`)
VALUES (1, 'Bamako', 'fr', 20, 1, 20, 'Quelle est la capitale du Mali ?', NULL, 5, 2, 1),
       (2, 'Les Twins', 'fr', 30, 2, 20, 'Qui sont ces artistes ?', 1, 4, 2, 1),
       (3, 'Gérald Darmanin', 'fr', 40, 3, 20, 'Donnez le nom complet de cet individu', 2, 4, 2, 1),
       (4, 'La France', 'fr', 20, 4, 10, 'Dans quel pays mange t-on le plus de sushi (trop bon) ?', NULL, 7, 2, 1),
       (6, 'The Quiz2', 'en', 10, 1, 30, 'Which quiz is a liar ?', NULL, 7, 2, 3),
       (7, 'The Quiz3', 'en', 10, 1, 30, 'Which quiz is worst than the Quiz2 ?', NULL, 7, 2, 4),
       (8, 'The Quiz4', 'en', 10, 1, 30, 'Which quiz don''t understand the meaning of title ?', NULL, 7, 2, 5),
       (9, 'Le Quiz 6', 'fr', 10, 1, 30, 'Quelle quiz français est meilleur que le Quiz 1 ?', NULL, 7, 2, 6),
       (10, 'Ribéry', 'fr', 10, 1, 30, 'Qui a sorti la réplique arrêtez de vous battez ?', NULL, 4, 2, 7),
       (11, 'Ningen', 'jp', 1000, 1, 30, 'Ko no yo ni ichiban warui ikimono wa ?', NULL, 2, 2, 8),
       (12, 'quizz', 'fr', 10, 1, 30, 'Quiz ou quizz ?', NULL, 7, 2, 9),
       (13, 'Hamlet', 'en', 10, 1, 30, 'To be or not to be ?', NULL, 6, 2, 10),
       (14, 'I guess running ?', 'en', 10, 2, 30, 'What is the first sport of the earth ?', NULL, 1, 2, 10),
       (15, 'Yes but not too much', 'en', 10, 3, 30, 'The dog can eat cat food ?', NULL, 2, 2, 10),
       (16, 'Oui', 'fr', 10, 1, 30, 'Répondez à la question du titre ?', NULL, 7, 2, 11),
       (17, 'C''est un OS basé sur le noyau linux et utilisé en grande partie par des smartphones mais pas que', 'fr',
        10, 1, 45,
        'Qu''est-ce qu''Android ?', NULL, 8, 2, 12),
       (18,
        'Une bibliothèques logicielles, un environnement d''exécution, un framework, et un lot d''applications de base',
        'fr', 15, 2, 45,
        'Si android n''est pas seulement un OS, c''est quoi d''autres?', NULL, 8, 2, 12),
       (19, 'A partir de quelle version Dalvik rest remplacée par ART(Android RunTime) ?', 'fr', 10, 3, 15,
        'A partir de la version 5', NULL, 8, 2, 12),
       (20, 'C''est la version de l''ensemble des bibliothèques standard, graphique etc.', 'fr', 17, 4, 40,
        'Qu''est ce que la version d''API ?', NULL, 8, 2, 12),
       (21,
        'Car c''est à l''utilisateur de changer de version d''OS, donc il faut prendre en compte l''OS le plus utilisé dans le monde',
        'fr', 20, 5, 30,
        'Pourquoi l''information de l''API Android est importante ?',
        NULL, 8, 2, 12),
       (22, '1, ou peut-être 1 + 1 = 11, et ça c''est beau', 'fr', 25, 1, 5, '1 + 1 = ?', NULL, 7, 2, 21),
       (23, 'C''est Bugdroid', 'fr', 30, 2, 10, 'Qui est-ce ?', 4, 8, 2, 21),
       (24, 'Qu''est ce que KCulture ?', 'fr', 50, 3, 15, 'DaCulture ou KCulture ?', NULL, 8, 2, 21);

INSERT
INTO `answer` (`id`, `content`, `question_id`)
VALUES (5, '2', 22),
       (6, '2', 22),
       (7, '11', 22),
       (8, 'Android', 23),
       (9, 'Bugdroid', 23),
       (10, 'Le truc d''Android', 23),
       (11, 'creator', 22),
       (12, 'creator', 23),
       (13, 'KCulture', 24),
       (14, 'KCulture', 24),
       (15, 'Le petit bac', 24),
       (16, 'creator', 24);

INSERT INTO `user` (`id`, `email`, `password`, `pseudo`, `avatar_id`)
VALUES (1, 'masa@gmail.com', 'secret', 'masa', 1),
       (2, 'lucas@gmail.com', 'secret', 'barder', 2),
       (3, 'jter@gmail.com', 'jerem', 'secret', 3),
       (4, 'creator@gmail.com', 'creatorTest', 'creatorSecret', NULL);

INSERT
INTO `selected_quiz` (`user_id`, `quiz_id`)
VALUES (1, 1),
       (1, 2);

/*INSERT INTO `lobby`(`id`, `password`, `is_closed`, `has_begun`, `created_at`, `quiz_id`, `creator_id`)
VALUES (1, '67c49adcdd1bfa2b12968d534588cd0d', false, false, NULL, 21, 4);*/

/*INSERT INTO `player`(`id`, `lobby_id`, `user_id`, `score`, `is_banned`, `has_voted`)
VALUES (1, 1, 1, 0, false, false),
       (2, 1, 2, 0, false, false),
       (3, 1, 3, 0, false, false),
       (4, 1, 4, 0, false, false);

INSERT INTO `vote` (`id`, `is_validate`, `user_id`, `lobby_id`, `answer_id`)
VALUES (1, true, 1, 1, 5),
       (2, true, 2, 1, 6),
       (3, true, 3, 1, 7),
       (4, true, 1, 1, 8),
       (5, true, 2, 1, 9),
       (6, true, 3, 1, 10),
       (7, true, 4, 1, 11),
       (8, true, 4, 1, 12),
       (9, true, 1, 1, 13),
       (10, true, 2, 1, 14),
       (11, true, 3, 1, 15),
       (12, true, 4, 1, 16); */
