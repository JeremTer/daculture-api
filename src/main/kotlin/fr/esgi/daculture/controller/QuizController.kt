package fr.esgi.daculture.controller


import fr.esgi.daculture.model.Question
import fr.esgi.daculture.model.Quiz
import fr.esgi.daculture.service.QuizService
import fr.esgi.daculture.service.quiz.GetAllQuizzes
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("/api/quiz")
class QuizController(
    private val getAllQuizzes: GetAllQuizzes,
    private val quizService: QuizService,
) {

    @GetMapping
    fun fundAll(
        @RequestParam(required = false) filter: String?,
        @RequestParam(required = false) value: String?,
        @RequestParam(required = false) isDescending: Boolean?,
        @RequestParam(required = false) userId: Long?,
        @RequestParam(required = false) isSelected: Boolean?,
    ): Set<Quiz> {
        try {
            return getAllQuizzes.execute(filter, value, isDescending, userId, isSelected)
        } catch (e: Exception) {
            when (e.message) {
                "NullUserIdSelectedQuizzes" -> throw ResponseStatusException(
                    HttpStatus.FORBIDDEN,
                    "${e.message} : can't select quizzes without user id"
                )
                else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
            }
        }
    }

    @GetMapping("/{quizId}/questions")
    fun findQuizQuestions(@PathVariable(value = "quizId") quizId: Long): Set<Question>? {
        try {
            return quizService.getQuestions(quizId)
        } catch (e: Exception) {
            when (e.message) {
                "QuizNotFound" -> throw ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    "${e.message} : The quiz with id $quizId does not exists"
                )
                else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
            }
        }
    }

    @GetMapping("/{quizId}/questions/ids")
    fun findQuizQuestionsIds(@PathVariable(value = "quizId") quizId: Long): List<Long?> {
        try {
            return quizService.getQuestionsIds(quizId)
        } catch (e: Exception) {
            when (e.message) {
                "QuestionsNotFound" -> throw ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    "${e.message} : The quiz with id $quizId does not have any questions"
                )
                else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
            }
        }
    }

    @GetMapping("/{quizId}")
    fun findQuiz(@PathVariable(value = "quizId") quizId: Long): Quiz? {
        try {
            return quizService.getQuizById(quizId)
        } catch (e: Exception) {
            when (e.message) {
                "QuizNotFound" -> throw ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    "${e.message} : The quiz with id $quizId does not exists"
                )
                else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
            }
        }
    }
}
