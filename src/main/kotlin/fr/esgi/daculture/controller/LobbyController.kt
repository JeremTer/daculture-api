package fr.esgi.daculture.controller

import fr.esgi.daculture.model.*
import fr.esgi.daculture.service.LobbyService
import fr.esgi.daculture.service.lobby.GetVotesOfLobby
import fr.esgi.daculture.service.lobby.UpdateVotesOfLobby
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("/api/lobby")
class LobbyController(
        private val lobbyService: LobbyService,
        private val getVotesOfLobby: GetVotesOfLobby,
        private val updateVotesOfLobby: UpdateVotesOfLobby
) {

    @GetMapping("/{lobbyId}/{password}")
    fun findLobby(
            @PathVariable(value = "lobbyId") lobbyId: Long?,
            @PathVariable(value = "password") password: String?,
    ): Lobby? {
        try {
            return lobbyService.getById(lobbyId, password)
        } catch (e: Exception) {
            when (e.message) {
                "LobbyNotFound" -> throw ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        "${e.message} : The lobby with id $lobbyId and password $password does not exists"
                )
                "OldLobby" -> throw ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        "${e.message} : The lobby with id $lobbyId is finished"
                )
                "PasswordNull" -> throw ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        "${e.message} : You need to indicate the password"
                )
                else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
            }
        }
    }

    @PostMapping
    fun createNewLobby(
            @RequestBody lobby: Lobby?,
    ): Lobby? {
        try {
            return lobbyService.newLobby(lobby)
        } catch (e: Exception) {
            when (e.message) {
                "LobbyNull" -> throw ResponseStatusException(
                        HttpStatus.FORBIDDEN,
                        "${e.message} : Can not insert null Lobby data"
                )
                "PasswordNull" -> throw ResponseStatusException(
                        HttpStatus.FORBIDDEN,
                        "${e.message} : You need to define a password"
                )
                "CreatorNotFound" -> throw ResponseStatusException(
                        HttpStatus.FORBIDDEN,
                        "${e.message} : Creator id with id ${lobby?.creator?.id} does not correspond to any user"
                )
                "QuizNotFound" -> throw ResponseStatusException(
                        HttpStatus.FORBIDDEN,
                        "${e.message} : The quiz with id ${lobby?.quiz?.id} does not exists"
                )
                else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
            }
        }
    }

    @PutMapping
    fun updateLobby(
            @RequestBody lobby: Lobby?,
    ): Lobby? {
        try {
            return lobbyService.updateLobby(lobby)
        } catch (e: Exception) {
            when (e.message) {
                "LobbyNotFound" -> throw ResponseStatusException(
                        HttpStatus.FORBIDDEN,
                        "${e.message} : Lobby with id ${lobby?.id} does not exists"
                )
                else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
            }
        }
    }

    @DeleteMapping("/{lobbyId}")
    fun closeLobby(
            @PathVariable(value = "lobbyId") lobbyId: Long?,
    ): Lobby? {
        try {
            return lobbyService.closeLobby(lobbyId)
        } catch (e: Exception) {
            when (e.message) {
                "LobbyNotFound" -> throw ResponseStatusException(
                        HttpStatus.FORBIDDEN,
                        "${e.message} : Can not find lobby with id $lobbyId"
                )
                else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
            }
        }
    }

    @DeleteMapping("/player/{playerId}")
    fun removePlayer(
            @PathVariable(value = "playerId") playerId: Long?,
    ) {
        try {
            return lobbyService.removePlayer(playerId)
        } catch (e: Exception) {
            when (e.message) {
                "PlayerNotFound" -> throw ResponseStatusException(
                        HttpStatus.FORBIDDEN,
                        "${e.message} : Can not find player with id $playerId"
                )
                else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
            }
        }
    }

    @GetMapping("/{lobbyId}/votes")
    fun findVotesOfLobby(
            @PathVariable(value = "lobbyId") lobbyId: Long?
    ): Set<Vote>? {
        try {
            return getVotesOfLobby.execute(lobbyId)
        } catch (e: Exception) {
            when (e.message) {
                "NullLobbyId" -> throw ResponseStatusException(
                        HttpStatus.FORBIDDEN,
                        "${e.message}: the id of lobby is null"
                )
                "NotLobbyExist" -> throw ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        "${e.message}: Lobby with id '$lobbyId' not found"
                )
                "NullVotes" -> throw ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        "${e.message}: Votes not found for lobby with id '$lobbyId'"
                )
                "AtLeastOneVoteNoUser" -> throw ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        "${e.message}: At least one vote has no user"
                )
                "AtLeastOneVoteNoLobby" -> throw ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        "${e.message}: At least one vote has no lobby"
                )
                "AtLeastOneVoteNoAnswer" -> throw ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                    "${e.message}: At least one vote has no answer"
                )
                "AtLeastOneAnswerNoQuestion" -> throw ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    "${e.message}: At least one answer has no question"
                )
                else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
            }
        }
    }

    @GetMapping("/{lobbyId}/answers")
    fun findAnswersByLobby(
        @PathVariable(value = "lobbyId") lobbyId: Long,
    ): List<Answer>? {
        try {
            return lobbyService.findAnswers(lobbyId)
        } catch (e: Exception) {
            when (e.message) {
                "LobbyNotFound" -> throw ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    "${e.message}: Lobby with id $lobbyId not found"
                )
                else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
            }
        }
    }

    @PutMapping("/{lobbyId}/votes")
    fun updateVotesOfLobby(
        @PathVariable(value = "lobbyId") lobbyId: Long?,
        @RequestBody votes: List<Vote>?,
    ): Long? {
        try {
            return updateVotesOfLobby.execute(lobbyId, votes)
        } catch (e: Exception) {
            println(e.message)
            when(e.message) {
                "NullLobbyId" -> throw ResponseStatusException(
                        HttpStatus.FORBIDDEN,
                        "${e.message}: the id of lobby is null"
                )
                "NotLobbyExist" -> throw ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        "${e.message}: Lobby with id '$lobbyId' not found"
                )
                "NullVotes" -> throw ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        "${e.message}: Votes not found for lobby with id '$lobbyId'"
                )
                "NotCorrectGivenVotes" -> throw ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        "${e.message}: Given votes are not corrects for lobby with id '$lobbyId'"
                )
                else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
            }
        }
    }
}
