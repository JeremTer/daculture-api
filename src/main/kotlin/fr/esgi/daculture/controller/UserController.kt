package fr.esgi.daculture.controller

import fr.esgi.daculture.model.*
import fr.esgi.daculture.service.AnswerService
import fr.esgi.daculture.service.LobbyService
import fr.esgi.daculture.service.UserService
import fr.esgi.daculture.service.VoteService
import fr.esgi.daculture.service.user.SelectQuizzes
import fr.esgi.daculture.service.user.UnselectQuizzes
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("/api/user")
class UserController(
    private val userService: UserService,
    private val selectQuizzes: SelectQuizzes,
    private val unselectQuizzes: UnselectQuizzes,
    private val answerService: AnswerService,
    private val lobbyService: LobbyService,
    private val voteService: VoteService,
) {

    @GetMapping
    fun findAll() = userService.getAllUsers()

    @PostMapping("subscribe")
    fun subscribe(@RequestBody user: User?): Session? {
        try {
            return userService.subscribe(user)
        } catch (e: Exception) {
            when (e.message) {
                "UserNull" -> throw ResponseStatusException(
                    HttpStatus.FORBIDDEN,
                    "${e.message} : Can't add null User data"
                )
                "EmailAndPasswordRequired" -> throw ResponseStatusException(
                    HttpStatus.FORBIDDEN,
                    "${e.message} : User email and password required"
                )
                "UserAlreadyExist" -> throw ResponseStatusException(
                    HttpStatus.FORBIDDEN,
                    "${e.message} : The user with ${user?.email} already exist"
                )
                else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
            }
        }
    }

    @PostMapping("login")
    fun login(@RequestBody user: User?): Session? {
        try {
            return userService.login(user)
        } catch (e: Exception) {
            when (e.message) {
                "EmailAndPasswordRequired" -> throw ResponseStatusException(
                    HttpStatus.FORBIDDEN,
                    "${e.message} : User email and password required"
                )
                "UserNotSubscribe" -> throw ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    "${e.message} : The user with ${user?.email} is not subscribe"
                )
                else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
            }
        }
    }

    @DeleteMapping("logout/{userId}")
    fun logout(@PathVariable userId: Long): Unit? {
        try {
            return userService.logout(userId)
        } catch (e: Exception) {
            when (e.message) {
                "UserNotSubscribe" -> throw ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    "${e.message} : The user with id $userId is not subscribe"
                )
                "UserNotLogin" -> throw ResponseStatusException(
                    HttpStatus.FORBIDDEN,
                    "${e.message} : The user with id $userId is not login"
                )
                else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
            }
        }
    }

    @GetMapping("/{userId}/score")
    fun getScoreOfOnUser(@PathVariable userId: Long): Long {
        try {
            return userService.getScore(userId)
        } catch (e: Exception) {
            when (e.message) {
                "UserNotSubscribe" -> throw ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    "${e.message} : The user with id $userId is not subscribe"
                )
                else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
            }
        }
    }

    @GetMapping("/{userIds}/lobby/{lobbyId}/score")
    fun getScoreOfOneUserForOneLobby(
        @PathVariable(value = "userIds") userIds: List<Long>,
        @PathVariable(value = "lobbyId") lobbyId: Long,
    ): List<Player>? {

        try {
            return userIds.map { userId ->
                userService.getScoreByUserAndLobby(userId, lobbyId)!!
            }.sortedByDescending { player -> player.score }
        } catch (e: Exception) {
            when (e.message) {
                else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
            }
        }
    }

    @GetMapping("/{userId}/quizzes")
    fun findSelectedQuizzes(@PathVariable(value = "userId") userId: Long): Set<Quiz>? {
        try {
            return userService.getSelectedQuizzes(userId)
        } catch (e: Exception) {
            when (e.message) {
                "UserNotFound" -> throw ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    "${e.message} : The user with id $userId does not exists"
                )
                else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
            }
        }
    }

    @GetMapping("/{userIds}")
    fun getUsersByIds(@PathVariable(value = "userIds") userIds: List<Long>): List<User>? {
        try {
            return userService.getUsersByIds(userIds)
        } catch (e: Exception) {
            when (e.message) {
                "UserNotFound" -> throw ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    "${e.message} : Wrong user id"
                )
                else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
            }
        }
    }

    @PostMapping("/{userId}/quiz/select")
    fun selectQuizzes(
        @PathVariable(value = "userId") userId: Long?,
        @RequestBody quizzesToSelect: Set<Quiz>?,
    ): Set<Quiz> {
        return try {
            selectQuizzes.execute(userId, quizzesToSelect)
        } catch (e: Exception) {
            quizzesResponseException(e, userId)
            emptySet()
        }
    }

    @DeleteMapping("/{userId}/quiz/unselect")
    fun unselectQuizzes(
        @PathVariable(value = "userId") userId: Long?,
        @RequestBody quizzesToUnselect: Set<Quiz>?,
    ): Set<Quiz> {
        return try {
            unselectQuizzes.execute(userId, quizzesToUnselect)
        } catch (e: Exception) {
            quizzesResponseException(e, userId)
            emptySet()
        }
    }

    private fun quizzesResponseException(e: Exception, userId: Long?) {
        when (e.message) {
            "UserNotFound" -> throw ResponseStatusException(
                HttpStatus.NOT_FOUND,
                "${e.message} : The user with id $userId does not exists"
            )
            "EmptyList" -> throw ResponseStatusException(
                HttpStatus.FORBIDDEN,
                "${e.message} : The list is empty"
            )
            "UnknownAllQuizzesIds" -> throw ResponseStatusException(
                HttpStatus.FORBIDDEN,
                "${e.message} : The list of quizzes have to be not null"
            )
            "UnknownFewQuizzesIds" -> throw ResponseStatusException(
                HttpStatus.NOT_FOUND,
                "${e.message} : Few quizzes ids not found"
            )
            else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
        }
    }

    @PostMapping("/{userId}/lobby/{lobbyId}/answer")
    fun insertAnswers(
        @PathVariable(value = "userId") userId: Long?,
        @PathVariable(value = "lobbyId") lobbyId: Long?,
        @RequestBody answers: List<Answer>?,
    ): Set<Vote> {
        return try {
            answerService.insertAnswers(answers)
            lobbyService.closeTheLobby(lobbyId, userId)
            voteService.insertVotes(userId, lobbyId, answers)
        } catch (e: Exception) {
            when (e.message) {
                "UserNotFound" -> throw ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    "${e.message} : The user with id $userId does not exists"
                )
                "LobbyNotFound" -> throw ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    "${e.message} : The lobby with id $lobbyId does not exists"
                )
                else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
            }
        }
    }


}
