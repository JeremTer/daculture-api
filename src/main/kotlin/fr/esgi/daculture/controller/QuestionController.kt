package fr.esgi.daculture.controller

import fr.esgi.daculture.model.Answer
import fr.esgi.daculture.model.Question
import fr.esgi.daculture.model.QuestionCategory
import fr.esgi.daculture.service.QuestionService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("/api/question")
class QuestionController(private val questionService: QuestionService) {
    @GetMapping("/answer/{questionIds}")
    fun findAllByQuestions(@PathVariable(value = "questionIds") questionIds: List<Long>): List<Answer>? {
        try {
            return questionService.getAnswersByQuestions(questionIds)
        } catch (e: Exception) {
            when (e.message) {
                "QuestionNotFound" -> throw ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    "${e.message} : The question does not exists"
                )
                else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
            }
        }
    }

    @GetMapping("/category/{categoryIds}")
    fun getCategoryName(@PathVariable(value = "categoryIds") categoryIds: List<Long>): List<QuestionCategory> {
        try {
            return questionService.getCategoryByIds(categoryIds)
        } catch(e: Exception) {
            throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
        }
    }

    @GetMapping("/{questionIds}")
    fun fundAllByIds(@PathVariable(value = "questionIds") questionIds: List<Long>): List<Question>? {
        try {
            return questionService.getByIds(questionIds)
        } catch (e: Exception) {
            when (e.message) {
                "QuestionsNotFound" -> throw ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    "${e.message} : Questions not found"
                )
                else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
            }
        }
    }
}
