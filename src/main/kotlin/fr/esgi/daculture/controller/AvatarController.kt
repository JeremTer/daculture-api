package fr.esgi.daculture.controller

import fr.esgi.daculture.model.Avatar
import fr.esgi.daculture.service.AvatarService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("/api/avatar")
class AvatarController(private val avatarService: AvatarService) {

    @GetMapping
    fun findAll(): Set<Avatar> {
        try {
            return avatarService.getAllAvatars()
        } catch (e: Exception) {
            throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
        }
    }
}