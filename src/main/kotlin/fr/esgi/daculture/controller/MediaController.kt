package fr.esgi.daculture.controller

import fr.esgi.daculture.model.Media
import fr.esgi.daculture.service.MediaService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("/api/media")
class MediaController(private val mediaService: MediaService) {

    @GetMapping("/{mediaIds}")
    fun fundAllByIds(@PathVariable(value = "mediaIds") mediaIds: List<Long>): List<Media>? {
        try {
            return mediaService.getByIds(mediaIds)
        } catch (e: Exception) {
            when (e.message) {
                "MediasNotFound" -> throw ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    "${e.message} : Medias not found"
                )
                else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
            }
        }
    }
}
