package fr.esgi.daculture.controller


import fr.esgi.daculture.model.Player
import fr.esgi.daculture.service.PlayerService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("/api/player")
class PlayerController(private val playerService: PlayerService) {

    @PostMapping("")
    fun insert(
        @RequestBody player: HashMap<String, Long>?,
    ): Player? {
        try {
            return playerService.insert(player?.get("user"), player?.get("lobby"))
        } catch (e: Exception) {
            when (e.message) {
                "LobbyNotFound" -> throw ResponseStatusException(
                    HttpStatus.FORBIDDEN,
                    "${e.message} : Lobby with id ${player?.get("lobbyId")} does not exist"
                )
                "UserNotFound" -> throw ResponseStatusException(
                    HttpStatus.FORBIDDEN,
                    "${e.message} : User with id ${player?.get("userId")} does not exist"
                )
                "PlayerAlreadyInLobby" -> throw ResponseStatusException(
                    HttpStatus.FORBIDDEN,
                    "${e.message} : Player with user id ${player?.get("userId")} is already in the lobby !"
                )
                else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
            }
        }
    }

    @PutMapping("/ban")
    fun banPlayer(
        @RequestBody player: Player?,
    ): Player? {
        try {
            return playerService.banOrUnbanPlayer(player, true)
        } catch (e: Exception) {
            when (e.message) {
                "PlayerNotFound" -> throw ResponseStatusException(
                    HttpStatus.FORBIDDEN,
                    "${e.message} : Player with id ${player?.id} does not exist"
                )
                else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
            }
        }
    }

    @PutMapping("/unban")
    fun unbanPlayer(
        @RequestBody player: Player?,
    ): Player? {
        try {
            return playerService.banOrUnbanPlayer(player, false)
        } catch (e: Exception) {
            when (e.message) {
                "PlayerNotFound" -> throw ResponseStatusException(
                    HttpStatus.FORBIDDEN,
                    "${e.message} : Player with id ${player?.id} does not exist"
                )
                else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
            }
        }
    }

    @GetMapping("/lobby/{lobbyId}/hasVoted")
    fun hasEveryoneVoted(
        @PathVariable(value = "lobbyId") lobbyId: Long?,
    ): Set<Player>? {
        try {
            return playerService.hasEveryoneVoted(lobbyId)
        } catch (e: Exception) {
            when (e.message) {
                "LobbyNotFound" -> throw ResponseStatusException(
                    HttpStatus.FORBIDDEN,
                    "${e.message} : Lobby with id $lobbyId does not exist"
                )
                else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
            }
        }
    }

    @GetMapping("/lobby/{lobbyId}")
    fun getPlayers(
        @PathVariable(value = "lobbyId") lobbyId: Long?,
    ): List<Player>? {
        try {
            return playerService.getPlayers(lobbyId)
        } catch (e: Exception) {
            when (e.message) {
                "LobbyNotFound" -> throw ResponseStatusException(
                    HttpStatus.FORBIDDEN,
                    "${e.message} : Lobby with id $lobbyId does not exist"
                )
                else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
            }
        }
    }


}
