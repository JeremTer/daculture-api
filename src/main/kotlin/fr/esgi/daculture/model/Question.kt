package fr.esgi.daculture.model

import com.fasterxml.jackson.annotation.*
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import javax.persistence.*



@Entity
@Table(name = "question")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator::class, property = "id", scope = Question::class)
class Question {
    @get:Id
    @get:GeneratedValue(strategy = GenerationType.IDENTITY, generator = "native")
    @get:Column(name = "id", unique = true, nullable = false)
    open var id: Long? = null

    @get:Basic
    @get:Column(name = "wording", nullable = false, length = 255)
    open var wording: String? = null

    @get:Basic
    @get:Column(name = "good_answer", nullable = true)
    open var goodAnswer: String? = null

    @get:Basic
    @get:Column(name = "points", nullable = false)
    open var points: Int? = null

    @get:Basic
    @get:Column(name = "time", nullable = false)
    open var time: Int? = null

    @get:Basic
    @get:Column(name = "position", nullable = false)
    open var position: Int? = null

    @get:Basic
    @get:Column(name = "lang", nullable = false, length = 20)
    open var lang: String? = null

    @JsonIdentityReference(alwaysAsId = true)
    @get:ManyToOne(fetch = FetchType.EAGER, optional = true)
    @get:JoinColumn(name = "media_id")
    open var media: Media? = null

    @JsonIdentityReference(alwaysAsId = true)
    @get:ManyToOne(fetch = FetchType.EAGER, optional = false)
    @get:JoinColumn(name = "type_id")
    open var questionType: QuestionType? = null

    @JsonIdentityReference(alwaysAsId = true)
    @get:ManyToOne(fetch = FetchType.EAGER, optional = false)
    @get:JoinColumn(name = "category_id")
    open var questionCategory: QuestionCategory? = null

    @JsonIdentityReference(alwaysAsId = true)
    @get:ManyToOne(fetch = FetchType.EAGER, optional = false)
    @get:JoinColumn(name = "quiz_id")
    open var quiz: Quiz? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as Question
        if (id != that.id) return false
        return true
    }

    override fun hashCode(): Int {
        return if (id != null)
            id.hashCode()
        else 0
    }
}
