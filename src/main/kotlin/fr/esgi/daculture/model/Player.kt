package fr.esgi.daculture.model

import com.fasterxml.jackson.annotation.JsonIdentityInfo
import com.fasterxml.jackson.annotation.JsonIdentityReference
import com.fasterxml.jackson.annotation.ObjectIdGenerators
import javax.persistence.*

@Entity
@Table(name = "player")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator::class, property = "id", scope = Player::class)
open class Player {

    @get:Id
    @get:GeneratedValue(strategy = GenerationType.IDENTITY, generator = "native")
    @get:Column(name = "id", unique = true, nullable = false)
    open var id: Long? = null

    @JsonIdentityReference(true)
    @get:ManyToOne(fetch = FetchType.EAGER, optional = false)
    @get:JoinColumn(name = "lobby_id")
    open var lobby: Lobby? = null

    @JsonIdentityReference(true)
    @get:ManyToOne(fetch = FetchType.EAGER, optional = false)
    @get:JoinColumn(name = "user_id")
    open var user: User? = null

    @get:Basic
    @get:Column(name = "score", nullable = false)
    open var score: Int? = 0

    @get:Basic
    @get:Column(name = "is_banned", nullable = false)
    open var isBanned: Boolean? = false

    @get:Basic
    @get:Column(name = "has_voted", nullable = false)
    open var hasVoted: Boolean? = false

    override fun hashCode(): Int {
        return if (id != null)
            id.hashCode()
        else 0
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as Player
        if (id != that.id) return false
        return true
    }
}
