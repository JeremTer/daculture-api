package fr.esgi.daculture.model

import com.fasterxml.jackson.annotation.JsonIdentityInfo
import com.fasterxml.jackson.annotation.JsonIdentityReference
import com.fasterxml.jackson.annotation.ObjectIdGenerators
import javax.persistence.*

@Entity
@Table(name = "answer")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator::class, property = "id", scope = Answer::class)
open class Answer {

    @get:Id
    @get:GeneratedValue(strategy = GenerationType.IDENTITY, generator = "native")
    @get:Column(name = "id", unique = true, nullable = false)
    open var id: Long? = null

    @get:Basic
    @get:Column(name = "content", nullable = true, length = 255)
    open var content: String? = null

    @get:Column(name = "question_id")
    open var question: Long? = 0

    @get:OneToMany(mappedBy = "answer", cascade = arrayOf(CascadeType.ALL), fetch = FetchType.LAZY)
    open var votes: Set<Vote> = HashSet()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as Answer
        if (id != that.id) return false
        return true
    }

    override fun hashCode(): Int {
        return if (id != null)
            id.hashCode()
        else 0
    }

    override fun toString(): String {
        return "Answer(id=$id, content=$content, question=$question, votes=$votes)"
    }
}
