package fr.esgi.daculture.model

import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonIdentityInfo
import com.fasterxml.jackson.annotation.ObjectIdGenerators
import javax.persistence.*

@Entity
@Table(name = "session")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator::class, property = "id", scope = Session::class)
open class Session {
    @get:Id
    @get:GeneratedValue(strategy = GenerationType.IDENTITY, generator = "native")
    @get:Column(name = "id", unique = true, nullable = false)
    open var id: Long? = null

    @get:Basic
    @get:Column(name = "token", nullable = false)
    open var token: String? = null

    @get:ManyToOne(fetch = FetchType.LAZY, optional = true)
    @get:JoinColumn(name = "user_id")
    open var user: User? = null

    override fun hashCode(): Int {
        return if (id != null)
            id.hashCode()
        else 0
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as Session
        if (id != that.id) return false
        return true
    }

    override fun toString(): String {
        return "Session(" +
                "id : ${this.id}," +
                "token : ${this.token}, " +
                "userId : ${this.user?.id}" +
                ")"
    }
}
