package fr.esgi.daculture.model

import com.fasterxml.jackson.annotation.JsonIdentityInfo
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.ObjectIdGenerators
import javax.persistence.*

@Entity
@JsonIgnoreProperties("questions")
@Table(name = "question_category")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator::class,
    property = "id",
    scope = QuestionCategory::class)
open class QuestionCategory {
    @get:Id
    @get:GeneratedValue(strategy = GenerationType.IDENTITY, generator = "native")
    @get:Column(name = "id", unique = true, nullable = false)
    open var id: Long? = null

    @get:Basic
    @get:Column(name = "name", nullable = false, length = 255)
    open var name: String? = null

    @get:OneToMany(mappedBy = "questionCategory", cascade = arrayOf(CascadeType.ALL), fetch = FetchType.LAZY)
    open var questions: Set<Question> = HashSet()

    override fun hashCode(): Int {
        return if (id != null)
            id.hashCode()
        else 0
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as QuestionCategory
        if (id != that.id) return false
        return true
    }
}
