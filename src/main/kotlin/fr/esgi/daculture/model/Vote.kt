package fr.esgi.daculture.model

import com.fasterxml.jackson.annotation.JsonIdentityInfo
import com.fasterxml.jackson.annotation.JsonIdentityReference
import com.fasterxml.jackson.annotation.ObjectIdGenerators
import javax.persistence.*

@Entity
@Table(name = "vote")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator::class, property = "id", scope = Vote::class)
open class Vote {

    @get:Id
    @get:GeneratedValue(strategy = GenerationType.IDENTITY, generator = "native")
    @get:Column(name = "id", unique = true, nullable = false)
    open var id: Long? = null

    @get:Basic
    @get:Column(name = "is_validate", nullable = false)
    open var isValidate: Boolean? = true

    @JsonIdentityReference(alwaysAsId = true)
    @get:ManyToOne(fetch = FetchType.EAGER, optional = false)
    @get:JoinColumn(name = "user_id")
    open var user: User? = null

    @JsonIdentityReference(alwaysAsId = true)
    @get:ManyToOne(fetch = FetchType.EAGER, optional = false)
    @get:JoinColumn(name = "lobby_id")
    open var lobby: Lobby? = null

    @get:ManyToOne(fetch = FetchType.EAGER, optional = false)
    @get:JoinColumn(name = "answer_id")
    open var answer: Answer? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as Vote
        if (id != that.id) return false
        return true
    }

    override fun hashCode(): Int {
        return if (id != null)
            id.hashCode()
        else 0
    }

    override fun toString(): String {
        return "Vote(id=$id, isValidate=$isValidate, user=$user, lobby=$lobby, answer=$answer)"
    }


}
