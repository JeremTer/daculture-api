package fr.esgi.daculture.model

import com.fasterxml.jackson.annotation.JsonIdentityInfo
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.ObjectIdGenerators
import javax.persistence.*

@Entity
@JsonIgnoreProperties("users")
@Table(name = "avatar")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator::class, property = "id", scope = Avatar::class)
open class Avatar {
    @get:Id
    @get:GeneratedValue(strategy = GenerationType.IDENTITY, generator = "native")
    @get:Column(name = "id", unique = true, nullable = false)
    open var id: Long? = null

    @get:Basic
    @get:Column(name = "image", nullable = false)
    var image: String? = null

    @get:OneToMany(mappedBy = "avatar", cascade = arrayOf(CascadeType.ALL), fetch = FetchType.LAZY)
    open var users: Set<User> = HashSet()

    override fun hashCode(): Int {
        return if (id != null)
            id.hashCode()
        else 0
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as Avatar
        if (id != that.id) return false
        return true
    }
}
