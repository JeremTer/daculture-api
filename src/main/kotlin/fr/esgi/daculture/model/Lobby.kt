package fr.esgi.daculture.model

import com.fasterxml.jackson.annotation.*
import org.hibernate.annotations.CreationTimestamp
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "lobby")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator::class, property = "id", scope = Lobby::class)
open class Lobby {

    @get:Id
    @get:GeneratedValue(strategy = GenerationType.IDENTITY, generator = "native")
    @get:Column(name = "id", unique = true, nullable = false)
    open var id: Long? = null

    @get:Column(name = "password", nullable = false)
    open var password: String? = null

    @get:Column(name = "is_closed", nullable = false)
    open var isClosed: Boolean? = null

    @get:Column(name = "has_begun", nullable = false)
    open var hasBegun: Boolean? = null

    @get:Basic
    @get:Column(name = "createdAt", nullable = true, updatable = false)
    @get:CreationTimestamp
    open var createdAt: Date? = null

    //@JsonBackReference("lobby-quiz")
    @get:ManyToOne(fetch = FetchType.EAGER, optional = false)
    @get:JoinColumn(name = "quiz_id")
    open var quiz: Quiz? = null

    //@JsonBackReference("creator")
    @get:ManyToOne(fetch = FetchType.EAGER, optional = false)
    @get:JoinColumn(name = "creator_id")
    open var creator: User? = null

    @get:OneToMany(mappedBy = "lobby", cascade = arrayOf(CascadeType.ALL), fetch = FetchType.LAZY)
    open var votes: Set<Vote> = HashSet()

    //@JsonManagedReference("lobby")
    @get:OneToMany(mappedBy = "lobby", cascade = arrayOf(CascadeType.ALL), fetch = FetchType.LAZY)
    open var players: Set<Player> = HashSet()

    override fun hashCode(): Int {
        return if (id != null)
            id.hashCode()
        else 0
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as Lobby
        if (id != that.id) return false
        return true
    }

    override fun toString(): String {
        return "Lobby(id=$id, password=$password, isClosed=$isClosed, hasBegun=$hasBegun, createdAt=$createdAt)"
    }


}
