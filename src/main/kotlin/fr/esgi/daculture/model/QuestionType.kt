package fr.esgi.daculture.model

import com.fasterxml.jackson.annotation.*
import javax.persistence.*

@Entity
@JsonIgnoreProperties("questions")
@Table(name = "question_type")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator::class, property = "id", scope = QuestionType::class)
open class QuestionType {
    @get:Id
    @get:GeneratedValue(strategy = GenerationType.IDENTITY, generator = "native")
    @get:Column(name = "id", unique = true, nullable = false)
    open var id: Long? = null

    @get:Basic
    @get:Column(name = "name", nullable = false, length = 255)
    open var name: String? = null

    @get:OneToMany(mappedBy = "questionType", cascade = arrayOf(CascadeType.ALL), fetch = FetchType.LAZY)
    open var questions: Set<Question> = HashSet()

    override fun hashCode(): Int {
        return if (id != null)
            id.hashCode()
        else 0
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as QuestionType
        if (id != that.id) return false
        return true
    }
}
