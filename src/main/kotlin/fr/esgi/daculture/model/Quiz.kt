package fr.esgi.daculture.model

import com.fasterxml.jackson.annotation.*
import javax.persistence.*

@Entity
@JsonIgnoreProperties("lobbies")
@Table(name = "quiz")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator::class, property = "id", scope = Quiz::class)
open class Quiz() {
    @get:Id
    @get:GeneratedValue(strategy = GenerationType.IDENTITY, generator = "native")
    @get:Column(name = "id", unique = true, nullable = false)
    open var id: Long? = null

    @get:Basic
    @get:Column(name = "name", nullable = false, length = 255)
    open var name: String? = null

    @get:Basic
    @get:Column(name = "description", nullable = false)
    open var description: String? = null

    @get:Basic
    @get:Column(name = "rate", nullable = true)
    open var rate: Double? = null

    @get:Basic
    @get:Column(name = "lang", nullable = false, length = 20)
    open var lang: String? = null

    //@JsonManagedReference("lobby-quiz")
    @get:OneToMany(mappedBy = "quiz", cascade = arrayOf(CascadeType.ALL), fetch = FetchType.LAZY)
    open var lobbies: Set<Lobby> = HashSet()

    //@JsonManagedReference("quiz")
    @get:OneToMany(mappedBy = "quiz", cascade = arrayOf(CascadeType.ALL), fetch = FetchType.LAZY)
    open var questions: Set<Question> = HashSet()

    @get:ManyToMany(mappedBy = "quizzes", fetch = FetchType.LAZY, cascade = arrayOf(CascadeType.PERSIST))
    open var users: Set<User> = HashSet()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as Quiz
        if (id != that.id) return false
        return true
    }

    override fun hashCode(): Int {
        return if (id != null)
            id.hashCode()
        else 0
    }

    override fun toString(): String {
        return "Quiz(" +
                "id : ${this.id}," +
                "name : ${this.name}, " +
                "description : ${this.description}, " +
                "rate : ${this.rate}, " +
                "lang : ${this.lang}, " +
                "lobbies : ${this.lobbies}, " +
                "questions : ${this.questions}, " +
                "users : ${this.users}" +
                ")"
    }
}
