package fr.esgi.daculture.model

import com.fasterxml.jackson.annotation.*
import javax.persistence.*

@Entity
@JsonIgnoreProperties("sessions")
@Table(name = "user")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator::class, property = "id", scope = User::class)
open class User() {
    @get:Id
    @get:GeneratedValue(strategy = GenerationType.IDENTITY, generator = "native")
    @get:Column(name = "id", unique = true, nullable = false)
    open var id: Long? = null

    @get:Basic
    @get:Column(name = "email", nullable = false, length = 255)
    open var email: String? = null

    @get:Basic
    @get:Column(name = "password", nullable = false, length = 255)
    @get:JsonIgnore
    @set:JsonProperty(value="password")
    open var password: String? = null

    @get:Basic
    @get:Column(name = "pseudo", nullable = true, length = 50)
    open var pseudo: String? = null

    @get:ManyToOne(fetch = FetchType.LAZY, optional = true)
    @get:JoinColumn(name = "avatar_id")
    open var avatar: Avatar? = null

    //@JsonManagedReference("creator")
    @get:OneToMany(mappedBy = "creator", cascade = arrayOf(CascadeType.ALL), fetch = FetchType.LAZY)
    open var lobbies: Set<Lobby> = HashSet()

    @get:OneToMany(mappedBy = "user", cascade = arrayOf(CascadeType.ALL), fetch = FetchType.LAZY)
    open var sessions: Set<Session> = HashSet()

    @get:OneToMany(mappedBy = "user", cascade = arrayOf(CascadeType.ALL), fetch = FetchType.LAZY)
    open var votes: Set<Vote> = HashSet()

    //@JsonManagedReference("user")
    @get:OneToMany(mappedBy = "user", cascade = arrayOf(CascadeType.ALL), fetch = FetchType.LAZY)
    open var players: Set<Player> = HashSet()


    @get:ManyToMany(fetch = FetchType.LAZY, cascade = arrayOf(CascadeType.PERSIST))
    @get:JoinTable(
            name = "selected_quiz",
            joinColumns = [JoinColumn(name = "user_id")],
            inverseJoinColumns = [JoinColumn(name = "quiz_id")]
    )
    open var quizzes: Set<Quiz> = HashSet()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as User
        if (id != that.id) return false
        return true
    }

    override fun hashCode(): Int {
        return if (id != null)
            id.hashCode()
        else 0
    }

    override fun toString(): String {
        return "User(" +
                "id : ${this.id}," +
                "email : ${this.email}, " +
                "password : ${this.password}, " +
                "pseudo : ${this.pseudo}, " +
                "avatar : ${this.avatar}, " +
                "sessions : ${this.sessions}, " +
                "votes : ${this.votes}, " +
                "players : ${this.players}, " +
                ")"
    }


}
