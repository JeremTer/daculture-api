package fr.esgi.daculture.model

import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonIdentityInfo
import com.fasterxml.jackson.annotation.JsonManagedReference
import com.fasterxml.jackson.annotation.ObjectIdGenerators
import javax.persistence.*

@Entity
@Table(name = "media")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator::class, property = "id", scope = Media::class)
open class Media {
    @get:Id
    @get:GeneratedValue(strategy = GenerationType.IDENTITY, generator = "native")
    @get:Column(name = "id", unique = true, nullable = false)
    open var id: Long? = null

    @get:Basic
    @get:Column(name = "source", nullable = false)
    open var source: String? = null

    // @JsonManagedReference("media")
    @get:OneToMany(mappedBy = "media", cascade = arrayOf(CascadeType.ALL), fetch = FetchType.LAZY)
    open var questions: Set<Question> = HashSet()

    override fun hashCode(): Int {
        return if (id != null)
            id.hashCode()
        else 0
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as Player
        if (id != that.id) return false
        return true
    }
}
