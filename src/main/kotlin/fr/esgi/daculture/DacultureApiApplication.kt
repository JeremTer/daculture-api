package fr.esgi.daculture

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DacultureApiApplication

fun main(args: Array<String>) {
    runApplication<DacultureApiApplication>(*args)
}


