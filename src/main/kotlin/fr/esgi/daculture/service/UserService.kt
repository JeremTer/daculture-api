package fr.esgi.daculture.service

import fr.esgi.daculture.dao.PlayerDao
import fr.esgi.daculture.dao.SessionDao
import fr.esgi.daculture.model.User
import fr.esgi.daculture.dao.UserDao
import fr.esgi.daculture.model.Player
import fr.esgi.daculture.model.Quiz
import fr.esgi.daculture.model.Session
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.util.DigestUtils
import java.time.LocalDateTime

@Service
class UserService(private val userDao: UserDao, private val sessionDao: SessionDao, private val playerDao: PlayerDao) {

    fun getAllUsers(): List<User> {
        return userDao.findAll()
    }

    fun getSelectedQuizzes(userId: Long): Set<Quiz>? {
        val user = userDao.findByIdOrNull(userId) ?: throw Exception("UserNotFound")
        return user.quizzes
    }

    fun subscribe(user: User?): Session? {
        if (user?.email == null || user.password == null) {
            throw Exception("EmailAndPasswordRequired")
        }
        val checkUser = userDao.findByEmail(user.email)
        if (checkUser != null) {
            throw Exception("UserAlreadyExist")
        }
        val newUser = userDao.save(user)
        return createSession(newUser)
    }

    private fun createSession(user: User?): Session? {
        val token = DigestUtils.md5DigestAsHex("${LocalDateTime.now()}+${user?.email}".encodeToByteArray())
        val newSession = Session()
        newSession.token = token
        newSession.user = user
        return sessionDao.save(newSession)
    }

    fun login(user: User?): Session? {
        if (user?.email == null || user.password == null) {
            throw Exception("EmailAndPasswordRequired")
        }
        val checkUser = userDao.findByEmailAndPassword(user.email, user.password) ?: throw Exception("UserNotSubscribe")
        if (checkUser.sessions.isNotEmpty()) {
            sessionDao.deleteInBatch(checkUser.sessions)
        }
        return createSession(checkUser)
    }

    fun logout(userId: Long): Unit? {
        val checkUser = userDao.findByIdOrNull(userId) ?: throw Exception("UserNotSubscribe")
        if (checkUser.sessions.isEmpty()) {
            throw Exception("UserNotLogin")
        }
        return sessionDao.deleteInBatch(checkUser.sessions)
    }

    fun getScore(userId: Long): Long {
        val checkUser = userDao.findByIdOrNull(userId) ?: throw Exception("UserNotSubscribe")
        return getUserGlobalScore(checkUser)
    }

    fun getScoreByUserAndLobby(userId: Long, lobbyId: Long): Player? {
        return playerDao.findByUser_IdAndLobby_Id(userId, lobbyId)
    }

    private fun getUserGlobalScore(user: User): Long {
        return user.players.stream()
            .map(Player::score)
            .mapToLong { score -> score?.toLong() ?: 0L }
            .sum()
    }

    fun getUsersByIds(userIds: List<Long>): List<User>? {
        return userDao.findAllById(userIds)
    }
}
