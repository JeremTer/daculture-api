package fr.esgi.daculture.service

import fr.esgi.daculture.dao.AnswerDao
import fr.esgi.daculture.dao.QuestionCategoryDao
import fr.esgi.daculture.dao.QuestionDao
import fr.esgi.daculture.model.Answer
import fr.esgi.daculture.model.Question
import fr.esgi.daculture.model.QuestionCategory
import org.springframework.stereotype.Service

@Service
class QuestionService(private val questionDao: QuestionDao, private val answerDao: AnswerDao, private val questionCategoryDao: QuestionCategoryDao) {
    fun getByIds(questionIds: List<Long>): List<Question>? {
        val questions = questionDao.findAllById(questionIds)
        if (questions.isEmpty()) throw Exception("QuestionsNotFound")
        return questions
    }

    fun getAnswersByQuestions(questionsIds: List<Long>): List<Answer>? {
        return answerDao.findAllByQuestions(questionsIds)
    }


    fun getCategoryByIds(categoryIds: List<Long>): List<QuestionCategory> {
        return questionCategoryDao.findAllById(categoryIds)
    }
}
