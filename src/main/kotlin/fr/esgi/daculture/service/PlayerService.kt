package fr.esgi.daculture.service

import fr.esgi.daculture.dao.LobbyDao
import fr.esgi.daculture.dao.PlayerDao
import fr.esgi.daculture.dao.UserDao
import fr.esgi.daculture.model.Player
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

@Service
class PlayerService(private val playerDao: PlayerDao, private val userDao: UserDao, private val lobbyDao: LobbyDao) {

    fun banOrUnbanPlayer(player: Player?, isBanned: Boolean?): Player? {
        if (player == null || player.user?.id == null || player.lobby?.id == null) throw Exception("PlayerNotFound")

        val playerToUpdate =
            playerDao.findByUser_IdAndLobby_Id(player.user?.id, player.lobby?.id) ?: throw Exception("PlayerNotFound")
        playerToUpdate.isBanned = isBanned
        return playerDao.save(playerToUpdate)
    }

    fun hasEveryoneVoted(lobbyId: Long?): Set<Player>? {
        if (lobbyId == null) throw Exception("LobbyNotFound")
        val lobby = lobbyDao.findByIdOrNull(lobbyId) ?: throw Exception("LobbyNotFound")
        return lobby.players.filter { player -> player.hasVoted == false }.toSet()
    }

    fun getPlayers(lobbyId: Long?): List<Player>? {
        return playerDao.findAllByLobby(lobbyId)
    }

    fun insert(userId: Long?, lobbyId: Long?): Player? {
        val lobby = lobbyDao.findByIdOrNull(lobbyId) ?: throw Exception("LobbyNotFound")
        val user = userDao.findByIdOrNull(userId) ?: throw Exception("UserNotFound")

        lobby.players.forEach { p ->
            if (p.lobby?.id === lobbyId &&
                p.user?.id === userId
            ) {
                return p
            }
        }
        val player = Player()
        player.lobby = lobby
        player.user = user
        return playerDao.save(player)
    }
}
