package fr.esgi.daculture.service

import fr.esgi.daculture.dao.LobbyDao
import fr.esgi.daculture.dao.PlayerDao
import fr.esgi.daculture.dao.QuizDao
import fr.esgi.daculture.dao.UserDao
import fr.esgi.daculture.model.Answer
import fr.esgi.daculture.model.Lobby
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.util.DigestUtils
import java.time.LocalDate
import java.util.*
import java.util.concurrent.TimeUnit


@Service
class LobbyService(
    private val playerDao: PlayerDao,
    private val lobbyDao: LobbyDao,
    private val userDao: UserDao,
    private val quizDao: QuizDao,
) {

    fun getById(lobbyId: Long?, password: String?): Lobby? {
        if (password.isNullOrBlank()) throw Exception("PasswordNull")
        println(encrypte(password))
        val lobby = lobbyDao.findByIdAndPassword(lobbyId, encrypte(password)) ?: throw Exception("LobbyNotFound")
        val diffInMillies = lobby.createdAt?.time?.minus(Date().time)?.let { Math.abs(it) }
        val diffInDays = diffInMillies?.let { TimeUnit.DAYS.convert(it, TimeUnit.MILLISECONDS) }

        if (diffInDays != null && diffInDays >= 2) {
            closeLobby(lobbyId)
            throw Exception("OldLobby")
        }
        return lobby
    }

    private fun encrypte(stringToEncrypte: String?): String {
        return DigestUtils.md5DigestAsHex("${LocalDate.now()}+${stringToEncrypte}".encodeToByteArray())
    }

    fun newLobby(lobby: Lobby?): Lobby? {
        if (lobby == null) throw Exception("LobbyNull")
        if (lobby.creator == null || lobby.creator?.id == null) throw Exception("CreatorNotFound")
        if (lobby.quiz == null || lobby.quiz?.id == null) throw Exception("QuizNotFound")
        if (lobby.password.isNullOrBlank()) throw Exception("PasswordNull")
        val creator = userDao.findByIdOrNull(lobby.creator?.id) ?: throw Exception("CreatorNotFound")
        val quiz = quizDao.findByIdOrNull(lobby.quiz?.id) ?: throw Exception("QuizNotFound")

        lobby.password = encrypte(lobby.password)
        lobby.creator = creator
        lobby.quiz = quiz
        return lobbyDao.save(lobby)
    }

    fun closeLobby(lobbyId: Long?): Lobby? {
        val lobby = lobbyDao.findByIdOrNull(lobbyId) ?: throw Exception("LobbyNotFound")
        lobby.isClosed = true
        return lobbyDao.save(lobby)
    }

    fun removePlayer(playerId: Long?) {
        val player = playerDao.findByIdOrNull(playerId) ?: throw Exception("PlayerNotFound")
        return playerDao.delete(player)
    }

    fun updateLobby(lobby: Lobby?): Lobby? {
        val currentLobby = lobbyDao.findByIdOrNull(lobby?.id) ?: throw Exception("LobbyNotFound")
        if (lobby?.isClosed != null) {
            currentLobby.isClosed = lobby.isClosed
        }
        if (lobby?.hasBegun != null) {
            currentLobby.hasBegun = lobby.hasBegun
        }
        return lobbyDao.save(currentLobby)
    }

    fun findAnswers(lobbyId: Long): List<Answer>? {
        val lobby = lobbyDao.findByIdOrNull(lobbyId) ?: throw Exception("LobbyNotFound")
        return lobbyDao.getAnswers(lobbyId)
    }

    fun closeTheLobby(lobbyId: Long?, userId: Long?) {
        val lobby = lobbyDao.findByIdOrNull(lobbyId) ?: throw Exception("LobbyNotFound")
        val user = userDao.findByIdOrNull(userId) ?: throw Exception("UserNotFound")
        if (lobby.creator?.id == user.id) {
            lobby.isClosed = true
            lobbyDao.save(lobby)
        }
    }
}
