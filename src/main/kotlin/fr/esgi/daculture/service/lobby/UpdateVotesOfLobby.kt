package fr.esgi.daculture.service.lobby

import fr.esgi.daculture.dao.LobbyDao
import fr.esgi.daculture.dao.PlayerDao
import fr.esgi.daculture.dao.QuestionDao
import fr.esgi.daculture.dao.VoteDao
import fr.esgi.daculture.model.Vote
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

@Service
class UpdateVotesOfLobby(
    val playerDao: PlayerDao,
    val lobbyDao: LobbyDao,
    val voteDao: VoteDao,
) {
    fun execute(lobbyId: Long?, votes: List<Vote>?): Long {
        lobbyId ?: throw Exception("NullLobbyId")
        votes ?: throw Exception("NullVotes")
        val lobby = lobbyDao.findByIdOrNull(lobbyId) ?: throw Exception("NotLobbyExist")

        votes.filter { vote -> vote.isValidate == true }
            .forEach { vote ->
                updatePlayersScore(vote, lobbyId)
            }

        return fetchAndUpdatedVotes(votes)
    }

    private fun updatePlayersScore(vote: Vote, lobbyId: Long) {

        val player = playerDao.findByVote(vote.id, lobbyId)

        val questionPoints = voteDao.getQuestionPointsByVote(vote.id)

        player.score = player.score?.plus(questionPoints)
        playerDao.save(player)
    }

    private fun fetchAndUpdatedVotes(votes: List<Vote>): Long {
        val votesIds = votes.map { vote -> vote.id }
        val checkedVotes = voteDao.findAllById(votesIds)
        if (isParamVotesNotSameSizeToApiVotes(votes, checkedVotes) == true) throw Exception("NotCorrectGivenVotes")

        updateVotesIsValidateProperties(checkedVotes, votes)

        return voteDao.saveAll(checkedVotes).size.toLong()
    }

    private fun isParamVotesNotSameSizeToApiVotes(votes: List<Vote>?, checkedVotes: MutableList<Vote>): Boolean? {
        if (votes != null) {
            return votes.size != checkedVotes.size
        }
        return null
    }

    private fun updateVotesIsValidateProperties(checkedVotes: List<Vote>, votes: List<Vote>) {
        checkedVotes.forEach { checkedVote ->
            checkedVote.isValidate = votes.find { vote ->
                checkedVote.id == vote.id
            }?.isValidate
        }
    }
}
