package fr.esgi.daculture.service.lobby

import fr.esgi.daculture.dao.LobbyDao
import fr.esgi.daculture.dao.VoteDao
import fr.esgi.daculture.model.Lobby
import fr.esgi.daculture.model.Vote
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

@Service
class GetVotesOfLobby(private val lobbyDao: LobbyDao, private val voteDao: VoteDao) {
    fun execute(lobbyId: Long?): Set<Vote>? {
        lobbyId ?: throw Exception("NullLobbyId")
        lobbyDao.findByIdOrNull(lobbyId)?: throw Exception("NotLobbyExist")
        val votes = voteDao.findVotesByLobby(lobbyId) ?: throw Exception("NullVotes")
        if (isAtLeastOneVoteNoUser(votes)) throw Exception("AtLeastOneVoteNoUser")
        if (isAtLeastOneVoteHaveNoLobby(votes)) throw Exception("AtLeastOneVoteNoLobby")
        if (isAtLeastOneVoteHaveNoAnswer(votes)) throw Exception("AtLeastOneVoteNoAnswer")
        if (isAtLeastOneVoteHaveAnswerWithNoQuestion(votes)) throw Exception("AtLeastOneAnswerNoQuestion")

        return votes
    }

    private fun isAtLeastOneVoteNoUser(votes: Set<Vote>): Boolean {
        return votes.stream()
                .anyMatch { vote -> vote.user == null }
    }

    private fun isAtLeastOneVoteHaveNoLobby(votes: Set<Vote>): Boolean {
        return votes.stream()
                .anyMatch { vote -> vote.lobby == null }
    }

    private fun isAtLeastOneVoteHaveNoAnswer(votes: Set<Vote>): Boolean {
        return votes.stream()
                .anyMatch { vote -> vote.answer == null }
    }

    private fun isAtLeastOneVoteHaveAnswerWithNoQuestion(votes: Set<Vote>): Boolean {
        return votes.stream()
                .anyMatch { vote -> vote.answer?.question == null }
    }
}
