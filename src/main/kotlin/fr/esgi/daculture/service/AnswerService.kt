package fr.esgi.daculture.service

import fr.esgi.daculture.dao.AnswerDao
import fr.esgi.daculture.model.Answer
import fr.esgi.daculture.model.Lobby
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

@Service
class AnswerService(private val answerDao: AnswerDao) {
    fun insertAnswers(answers: List<Answer>?): Set<Answer> {
        if (answers.isNullOrEmpty()) {
            return setOf()
        }
        return answerDao.saveAll(answers).toSet()
    }
}
