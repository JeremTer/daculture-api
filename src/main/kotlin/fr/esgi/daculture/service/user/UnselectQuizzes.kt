package fr.esgi.daculture.service.user

import fr.esgi.daculture.dao.UserDao
import fr.esgi.daculture.model.Quiz
import fr.esgi.daculture.service.user.common.GetExistingQuizzes
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.util.stream.Collectors

@Service
class UnselectQuizzes(
        private val userDao: UserDao,
        private val getExistingQuizzes: GetExistingQuizzes
) {
    fun execute(userId: Long?, quizzesToUnselect: Set<Quiz>?): Set<Quiz> {
        val checkUser = userDao.findByIdOrNull(userId) ?: throw Exception("UserNotFound")
        val checkedQuizzes = getExistingQuizzes.execute(quizzesToUnselect)
        val quizzesWithoutIds= checkUser.quizzes.stream()
                .filter { quiz -> !checkedQuizzes.contains(quiz) }
                .collect(Collectors.toSet())
        checkUser.quizzes = quizzesWithoutIds
        userDao.save(checkUser)
        return checkUser.quizzes
    }
}