package fr.esgi.daculture.service.user.common

import fr.esgi.daculture.dao.QuizDao
import fr.esgi.daculture.model.Quiz
import org.springframework.stereotype.Service
import java.util.stream.Collectors

@Service
class GetExistingQuizzes(private val quizDao: QuizDao) {
    fun execute(listQuiz: Set<Quiz>?): Set<Quiz> {
        val listQuizzesIds = mapQuizzesToQuizzesIds(listQuiz)
        if (listQuizzesIds != null && listQuizzesIds.isEmpty()) {
            throw Exception("EmptyList")
        }
        val checkedQuizzes = listQuizzesIds?.let { quizDao.findAllById(listQuizzesIds) }?.toSet()
                ?: throw Exception("UnknownAllQuizzesIds")

        if (checkedQuizzes.size < listQuizzesIds.size) {
            throw Exception("UnknownFewQuizzesIds")
        }

        return checkedQuizzes
    }
    private fun mapQuizzesToQuizzesIds(listQuiz: Set<Quiz>?): Set<Long?>? {
        return listQuiz?.stream()
                ?.map { quiz -> quiz.id }
                ?.collect(Collectors.toSet())
    }
}