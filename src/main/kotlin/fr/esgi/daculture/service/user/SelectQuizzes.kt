package fr.esgi.daculture.service.user

import fr.esgi.daculture.dao.UserDao
import fr.esgi.daculture.model.Quiz
import fr.esgi.daculture.service.user.common.GetExistingQuizzes
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

@Service
class SelectQuizzes(
        private val userDao: UserDao,
        private val getExistingQuizzes: GetExistingQuizzes
) {
    fun execute(userId: Long?, quizzesToSelect: Set<Quiz>?): Set<Quiz> {
        val checkUser = userDao.findByIdOrNull(userId) ?: throw Exception("UserNotFound")

        checkUser.quizzes = mutableSetOf()
        if (quizzesToSelect != null && quizzesToSelect.isNotEmpty()) {
            checkUser.quizzes = checkUser.quizzes + getExistingQuizzes.execute(quizzesToSelect)
        }
        userDao.save(checkUser)

        return checkUser.quizzes
    }
}
