package fr.esgi.daculture.service

import fr.esgi.daculture.dao.AvatarDao
import fr.esgi.daculture.model.Avatar
import org.springframework.stereotype.Service

@Service
class AvatarService(private val avatarDao: AvatarDao) {
    fun getAllAvatars(): Set<Avatar> = avatarDao.findAll().toSet()
}
