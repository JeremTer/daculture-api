package fr.esgi.daculture.service

import fr.esgi.daculture.dao.MediaDao
import fr.esgi.daculture.model.Media
import org.springframework.stereotype.Service

@Service
class MediaService(private val mediaDao: MediaDao) {

    fun getByIds(mediaIds: List<Long>): List<Media>? {
        val medias = mediaDao.findAllById(mediaIds)
        if (medias.isEmpty()) throw Exception("MediasNotFound")
        return medias
    }


}
