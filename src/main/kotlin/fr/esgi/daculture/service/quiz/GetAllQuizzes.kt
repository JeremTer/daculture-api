package fr.esgi.daculture.service.quiz

import fr.esgi.daculture.dao.QuizDao
import fr.esgi.daculture.model.Quiz
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import java.util.stream.Collectors

@Service
class GetAllQuizzes(private val quizDao: QuizDao) {
    enum class FilterType(val value: String) {
        NAME("name"),
        LANG("lang"),
        DESCRIPTION("description"),
        RATE("rate")
    }

    fun execute(
            filter: String?,
            value: String?,
            isDescending: Boolean? = null,
            userId: Long? = null,
            isSelected: Boolean? = null,
    ): Set<Quiz> {
        if (isSelected != null && userId == null) {
            throw Exception("NullUserIdSelectedQuizzes")
        }
        val sortDirection = defineSortDirection(isDescending)
        val quizzes = findAllQuizzesByFilter(filter, sortDirection)
        return filterQuizzes(quizzes, filter, value, userId, isSelected)
    }

    private fun defineSortDirection(isDescending: Boolean?) = when (isDescending) {
        true -> Sort.Direction.DESC
        else -> Sort.Direction.ASC
    }

    private fun findAllQuizzesByFilter(filter: String?, sortDirection: Sort.Direction) = when (filter) {
        "name", "description", "lang" -> {
            quizDao.findAll(Sort.by(sortDirection, filter))
        }
        "rate" -> {
            quizDao.findAll(Sort.by(sortDirection, "rate"))
        }
        else -> quizDao.findAll(Sort.by(sortDirection, "id"))
    }

    private fun filterQuizzes(quizzes: List<Quiz>, filter: String?, value: String?, userId: Long?, isSelected: Boolean?): MutableSet<Quiz> {
        return quizzes.stream()
                .filter { filterQuizByValue(it, filter, value) }
                .filter { filterSelectedQuiz(it, userId, isSelected) }
                .collect(Collectors.toSet())
    }

    private fun filterQuizByValue(quiz: Quiz, filter: String?, value: String?): Boolean {
        return when (filter) {
            FilterType.NAME.value -> quizPropertyNotNullAndContainString(quiz.name, value)
            FilterType.LANG.value -> quizPropertyNotNullAndContainString(quiz.lang, value)
            FilterType.DESCRIPTION.value -> quizPropertyNotNullAndContainString(quiz.description, value)
            FilterType.RATE.value -> quizRateNotNullAndContainNumber(quiz.rate, value)
            else -> true
        }
    }

    private fun quizRateNotNullAndContainNumber(rate: Double?, value: String?): Boolean {
        return rate?.let {
            value?.toDoubleOrNull()?.let {rateValue ->
                rate == rateValue
            }
        } ?: true
    }

    private fun quizPropertyNotNullAndContainString(property: String?, value: String?): Boolean {
        return property?.let {
            value?.let {
                property.contains(value)
            }
        } ?: true
    }

    private fun filterSelectedQuiz(quiz: Quiz, userId: Long?, isSelected: Boolean?): Boolean {
        return when (userId) {
            is Long -> {
                when (isSelected) {
                    true -> isQuizIsSelectedByUser(quiz, userId)
                    false -> isQuizIsNotSelectedByUser(quiz, userId)
                    else -> true
                }
            }
            else -> true
        }
    }

    private fun isQuizIsSelectedByUser(quiz: Quiz, userId: Long?) = quiz.users.find { user ->
        user.id == userId
    } != null

    private fun isQuizIsNotSelectedByUser(quiz: Quiz, userId: Long?) = quiz.users.find { user ->
        user.id == userId
    } == null
}