package fr.esgi.daculture.service

import fr.esgi.daculture.dao.QuestionDao
import fr.esgi.daculture.dao.QuizDao
import fr.esgi.daculture.model.Question
import fr.esgi.daculture.model.Quiz
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

@Service
class QuizService(private val quizDao: QuizDao, private val questionDao: QuestionDao) {

    fun getQuestions(quizId: Long): Set<Question>? {
        val quiz = quizDao.findByIdOrNull(quizId) ?: throw Exception("QuizNotFound")
        return quiz.questions
    }

    fun getQuestionsIds(quizId: Long): List<Long?> {
        val questionIds = questionDao.findIdsByQuizId(quizId) ?: throw Exception("QuestionsNotFound")
        if (questionIds.isEmpty()) throw Exception("QuestionsNotFound")
        return questionIds
    }

    fun getQuizById(quizId: Long): Quiz? {
        return quizDao.findByIdOrNull(quizId) ?: throw Exception("QuizNotFound")
    }
}
