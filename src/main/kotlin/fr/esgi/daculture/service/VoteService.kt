package fr.esgi.daculture.service

import fr.esgi.daculture.dao.LobbyDao
import fr.esgi.daculture.dao.UserDao
import fr.esgi.daculture.dao.VoteDao
import fr.esgi.daculture.model.Answer
import fr.esgi.daculture.model.Vote
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

@Service
class VoteService(private val voteDao: VoteDao, private val userDao: UserDao, private val lobbyDao: LobbyDao) {
    fun insertVotes(userId : Long?, lobbyId: Long?, answers: List<Answer>? ): Set<Vote> {
        val user = userDao.findByIdOrNull(userId)
        val lobby = lobbyDao.findByIdOrNull(lobbyId)
        val votes = mutableListOf<Vote>()

        if (answers.isNullOrEmpty()) {
            return setOf()
        }

        answers.forEach { answer ->
            val vote = Vote()
            if (answer.content?.isEmpty() == true) {
                vote.isValidate = false
            }
            vote.answer = answer
            vote.user = user
            vote.lobby = lobby
            votes.add(vote)
        }

        return voteDao.saveAll(votes).toSet()
    }
}
