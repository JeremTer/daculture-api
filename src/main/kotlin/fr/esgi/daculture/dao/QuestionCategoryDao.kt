package fr.esgi.daculture.dao

import fr.esgi.daculture.model.QuestionCategory
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface QuestionCategoryDao : JpaRepository<QuestionCategory, Long> {
}
