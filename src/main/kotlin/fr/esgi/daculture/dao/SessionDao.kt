package fr.esgi.daculture.dao

import fr.esgi.daculture.model.Session
import fr.esgi.daculture.model.User
import org.springframework.data.jpa.repository.JpaRepository

interface SessionDao : JpaRepository<Session, Long> {
}