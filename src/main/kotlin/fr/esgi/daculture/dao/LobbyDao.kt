package fr.esgi.daculture.dao

import fr.esgi.daculture.model.Answer
import fr.esgi.daculture.model.Lobby
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface LobbyDao : JpaRepository<Lobby, Long> {

    fun findByIdAndPassword(lobbyId: Long?, password: String?): Lobby?

    @Query("SELECT a FROM Answer a INNER JOIN Vote v ON v.answer = a WHERE v.lobby.id = ?1")
    fun getAnswers(lobbyId: Long): List<Answer>?
}
