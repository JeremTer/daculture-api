package fr.esgi.daculture.dao

import fr.esgi.daculture.model.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface UserDao : JpaRepository<User, Long> {

    @Query("SELECT u FROM User u WHERE u.email = ?1")
    fun findByEmail(email: String?): User?

    @Query("SELECT u FROM User u WHERE u.email = ?1 AND u.password = ?2")
    fun findByEmailAndPassword(email: String?, password: String?): User?
}
