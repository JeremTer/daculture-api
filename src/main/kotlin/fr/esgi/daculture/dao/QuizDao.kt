package fr.esgi.daculture.dao

import fr.esgi.daculture.model.Quiz
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface QuizDao: JpaRepository<Quiz, Long> {
}
