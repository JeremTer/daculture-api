package fr.esgi.daculture.dao

import fr.esgi.daculture.model.Avatar
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface AvatarDao : JpaRepository<Avatar, Long> {
}