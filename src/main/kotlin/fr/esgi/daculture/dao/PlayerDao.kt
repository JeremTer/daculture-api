package fr.esgi.daculture.dao

import fr.esgi.daculture.model.Player
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface PlayerDao : JpaRepository<Player, Long> {
    fun findByUser_IdAndLobby_Id(userId: Long?, lobbyId: Long?): Player?

    @Query("SELECT * FROM player WHERE lobby_id = ?1 ", nativeQuery = true)
    fun findAllByLobby(lobbyId: Long?): List<Player>

    @Query("SELECT p FROM Player p WHERE p.user.id = ?1")
    fun findByUserId(userId: Long?): Player

    @Query("SELECT * FROM player p INNER JOIN user u ON p.user_id = u.id INNER JOIN vote v ON v.user_id = u.id WHERE v.id = ?1 AND p.lobby_id = ?2",
        nativeQuery = true)
    fun findByVote(voteId: Long?, lobbyId: Long): Player
}
