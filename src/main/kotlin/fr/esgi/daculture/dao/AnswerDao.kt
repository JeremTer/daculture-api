package fr.esgi.daculture.dao

import fr.esgi.daculture.model.Answer
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface AnswerDao : JpaRepository<Answer, Long> {

    @Query("SELECT * FROM answer WHERE question_id IN (?1)", nativeQuery = true)
    fun findAllByQuestions(questionsIds: List<Long>): List<Answer>?
}
