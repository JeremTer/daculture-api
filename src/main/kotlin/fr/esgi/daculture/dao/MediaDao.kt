package fr.esgi.daculture.dao

import fr.esgi.daculture.model.Media
import org.springframework.data.jpa.repository.JpaRepository

interface MediaDao : JpaRepository<Media, Long> {

}
