package fr.esgi.daculture.dao

import fr.esgi.daculture.model.Lobby
import fr.esgi.daculture.model.Vote
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface VoteDao : JpaRepository<Vote, Long> {

    @Query("SELECT * FROM vote WHERE lobby_id = ?1", nativeQuery = true)
    fun findVotesByLobby(lobbyId: Long?): Set<Vote>?

    //@Query("SELECT q.points FROM Question q WHERE q.id = (SELECT a.question FROM Vote v INNER JOIN v.answer a INNER JOIN a.question WHERE v.id = ?1)")

    @Query("Select q.points from vote v inner join answer a on v.answer_id = a.id inner join question q on q.id = a.question_id where v.id = ?1",
        nativeQuery = true)
    fun getQuestionPointsByVote(voteId: Long?): Int
}
