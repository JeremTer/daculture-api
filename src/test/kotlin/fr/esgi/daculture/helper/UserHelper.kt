package fr.esgi.daculture.helper

import fr.esgi.daculture.model.*

class UserHelper {
    companion object {
        fun createUser(id: Long?,
                       email: String?,
                       password: String?,
                       pseudo: String?,
                       avatar: Avatar? = null,
                       sessions: Set<Session> = mutableSetOf(),
                       votes: Set<Vote> = mutableSetOf(),
                       players: Set<Player> = mutableSetOf()
        ): User {
            val user = User()
            user.id = id
            user.email = email
            user.password = password
            user.pseudo = pseudo
            user.avatar = avatar
            user.sessions = sessions
            user.votes = votes
            user.players = players
            return user
        }
    }
}