package fr.esgi.daculture.helper

import fr.esgi.daculture.model.Answer
import fr.esgi.daculture.model.Vote

class AnswerHelper {
    companion object {
        fun createAnswer(
                id: Long?,
                content: String?,
                question: Long?,
                votes: Set<Vote> = mutableSetOf()
        ): Answer {
            val answer = Answer()
            answer.id = id
            answer.content = content
            answer.question = question
            answer.votes = votes
            return answer
        }
    }
}