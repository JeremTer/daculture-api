package fr.esgi.daculture.helper

import fr.esgi.daculture.model.*
import java.time.LocalDate
import java.util.*

class LobbyHelper {
    companion object {
        fun createLobby(id: Long?,
                        createAt: Date? = Date(LocalDate.now().toEpochDay()),
                        creator: User? = UserHelper.createUser(
                                id = 2,
                                email = "creator@gmail.com",
                                password = "creatorpwd",
                                pseudo = "creatorPseudo"
                        ),
                        password: String?,
                        quiz: Quiz?,
                        players: Set<Player> = mutableSetOf(),
                        votes: Set<Vote> = mutableSetOf()
                        ): Lobby {
            val lobby = Lobby()
            lobby.id = id
            lobby.createdAt = createAt
            lobby.creator = creator
            lobby.password = password
            lobby.players = players
            lobby.quiz = quiz
            lobby.votes = votes

            return lobby
        }
    }
}