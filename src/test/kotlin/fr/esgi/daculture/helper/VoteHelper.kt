package fr.esgi.daculture.helper

import fr.esgi.daculture.model.Answer
import fr.esgi.daculture.model.Lobby
import fr.esgi.daculture.model.User
import fr.esgi.daculture.model.Vote

class VoteHelper {
    companion object {
        fun createVote(
                id: Long?,
                isValidate: Boolean?,
                user: User?,
                lobby: Lobby?,
                answer: Answer?
        ): Vote {
            val vote = Vote()
            vote.id = id
            vote.isValidate = isValidate
            vote.user = user
            vote.lobby = lobby
            vote.answer = answer

            return vote
        }
    }
}