package fr.esgi.daculture.helper

import fr.esgi.daculture.model.Lobby
import fr.esgi.daculture.model.Question
import fr.esgi.daculture.model.Quiz
import fr.esgi.daculture.model.User

class QuestionHelper {
    companion object {
        fun createQuiz(id: Long?,
                       name: String?,
                       description: String?,
                       rate: Double? = 0.0,
                       lang: String? = "",
                       lobbies: Set<Lobby> = mutableSetOf(),
                       questions: Set<Question> = mutableSetOf(),
                       users: Set<User> = mutableSetOf()
        ): Quiz {
            val quiz = Quiz()
            quiz.id = id
            quiz.name = name
            quiz.description = description
            quiz.rate = rate
            quiz.lang = lang
            quiz.lobbies = lobbies
            quiz.questions = questions
            quiz.users = users
            return quiz
        }
    }
}