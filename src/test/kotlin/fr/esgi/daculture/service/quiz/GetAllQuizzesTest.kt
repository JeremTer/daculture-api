package fr.esgi.daculture.service.quiz

import fr.esgi.daculture.dao.QuizDao
import fr.esgi.daculture.helper.QuestionHelper
import fr.esgi.daculture.helper.UserHelper
import fr.esgi.daculture.model.Quiz
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.Sort

class GetAllQuizzesTest {
    private lateinit var getAllQuizzes: GetAllQuizzes

    @MockBean
    lateinit var quizDao: QuizDao

    @BeforeEach
    fun setup() {
        quizDao = Mockito.mock(QuizDao::class.java)
        getAllQuizzes = GetAllQuizzes(quizDao)
    }

    @Test
    fun `getAllQuizzes when quiz list is empty list should return empty list`() {
        val emptyList = mutableListOf<Quiz>()

        Mockito.`when`(quizDao.findAll()).thenReturn(emptyList)

        val result = getAllQuizzes.execute(
                null,
                null
        )
        assertThat(result.size).isEqualTo(0)
        assertThat(result.containsAll(emptyList)).isTrue
    }

    @Test
    fun `getAllQuizzes when database contain quizzes, should return all quizzes`() {
        val quizzes = mutableListOf<Quiz>()
        quizzes.add(QuestionHelper.createQuiz(1, name = "test", "description"))
        quizzes.add(QuestionHelper.createQuiz(2, name = "tests", "description"))
        quizzes.add(QuestionHelper.createQuiz(3, name = "abcd", "efgh"))
        val expectedQuizzes = mutableListOf<Quiz>()
        expectedQuizzes.add(QuestionHelper.createQuiz(1, name = "test", "description"));
        expectedQuizzes.add(QuestionHelper.createQuiz(2, name = "tests", "description"));
        expectedQuizzes.add(QuestionHelper.createQuiz(3, name = "abcd", "efgh"));

        Mockito.`when`(quizDao.findAll(Mockito.any(Sort::class.java))).thenReturn(quizzes)

        val result = getAllQuizzes.execute(
                null,
                null
        )
        assertThat(result.size).isEqualTo(3)
        assertThat(result.containsAll(expectedQuizzes)).isTrue
        assertThat(expectedQuizzes.containsAll(result)).isTrue
    }

    @Test
    fun `getAllQuizzes when filter is name should select quizzes by name that contain value`() {
        val quizzes = mutableListOf<Quiz>()
        quizzes.add(QuestionHelper.createQuiz(1, name = "test", "description"))
        quizzes.add(QuestionHelper.createQuiz(2, name = "tests", "description"))
        quizzes.add(QuestionHelper.createQuiz(3, name = "abcd", "efgh"))

        val expectedQuizzes = mutableListOf<Quiz>()
        expectedQuizzes.add(QuestionHelper.createQuiz(1, name = "test", "description"))
        expectedQuizzes.add(QuestionHelper.createQuiz(2, name = "tests", "description"))

        Mockito.`when`(quizDao.findAll(Mockito.any(Sort::class.java))).thenReturn(quizzes)

        val result = getAllQuizzes.execute(
                "name",
                "test"
        )
        assertThat(result.size).isEqualTo(2)
        assertThat(result.containsAll(expectedQuizzes)).isTrue
        assertThat(expectedQuizzes.containsAll(result)).isTrue
    }

    @Test
    fun `getAllQuizzes when filter is lang should select quizzes by lang that contain value`() {
        val quizzes = mutableListOf<Quiz>()
        quizzes.add(QuestionHelper.createQuiz(1, name = "test", "description", lang = "en"))
        quizzes.add(QuestionHelper.createQuiz(2, name = "tests", "description", lang = "fr"))
        quizzes.add(QuestionHelper.createQuiz(3, name = "abcd", "efgh"))
        val expectedQuizzes = mutableListOf<Quiz>()
        expectedQuizzes.add(QuestionHelper.createQuiz(1, name = "test", "description", lang = "en"))
        Mockito.`when`(quizDao.findAll(Mockito.any(Sort::class.java))).thenReturn(quizzes)

        val result = getAllQuizzes.execute(
                "lang",
                "en"
        )

        assertThat(result.size).isEqualTo(1)
        assertThat(result.containsAll(expectedQuizzes)).isTrue
        assertThat(expectedQuizzes.containsAll(result)).isTrue
    }

    @Test
    fun `getAllQuizzes when filter is description should select quizzes by description that contain value`() {
        val quizzes = mutableListOf<Quiz>()
        quizzes.add(QuestionHelper.createQuiz(1, name = "test", "rights"))
        quizzes.add(QuestionHelper.createQuiz(2, name = "tests", "left"))
        quizzes.add(QuestionHelper.createQuiz(3, name = "abcd", "right"))
        val expectedQuizzes = mutableListOf<Quiz>()
        expectedQuizzes.add(QuestionHelper.createQuiz(1, name = "test", "rights"))
        expectedQuizzes.add(QuestionHelper.createQuiz(3, name = "abcd", "right"))
        Mockito.`when`(quizDao.findAll(Mockito.any(Sort::class.java))).thenReturn(quizzes)

        val result = getAllQuizzes.execute(
                "description",
                "right"
        )
        assertThat(result.size).isEqualTo(2)
        assertThat(result.containsAll(expectedQuizzes)).isTrue
        assertThat(expectedQuizzes.containsAll(result)).isTrue
    }


    @Test
    fun `getAllQuizzes when isDescending is true and filter is description should order by description content`() {
        val quizzes = mutableListOf<Quiz>()
        quizzes.add(QuestionHelper.createQuiz(1, name = "test", "b rights place"))
        quizzes.add(QuestionHelper.createQuiz(2, name = "tests", "left"))
        quizzes.add(QuestionHelper.createQuiz(3, name = "abcd", "a right place"))

        getAllQuizzes.execute(
                "description",
                "right",
                isDescending = true
        )
        Mockito.verify(quizDao, Mockito.times(1))
                .findAll(Sort.by(Sort.Direction.DESC, "description"))
    }

    @Test
    fun `getAllQuizzes when isDescending is false and filter is description should order ascendingly by description content`() {
        val quizzes = mutableListOf<Quiz>()
        quizzes.add(QuestionHelper.createQuiz(1, name = "test", "rights place"))
        quizzes.add(QuestionHelper.createQuiz(2, name = "tests", "left"))
        quizzes.add(QuestionHelper.createQuiz(3, name = "abcd", "right place"))

        getAllQuizzes.execute(
                "description",
                "right",
                isDescending = false
        )
        Mockito.verify(quizDao, Mockito.times(1))
                .findAll(Sort.by(Sort.Direction.ASC, "description"))
    }

    @Test
    fun `getAllQuizzes when isDescending is false and filter is name should order by name content`() {
        val quizzes = mutableListOf<Quiz>()
        quizzes.add(QuestionHelper.createQuiz(1, name = "test", "rights"))
        quizzes.add(QuestionHelper.createQuiz(2, name = "tests", "left"))
        quizzes.add(QuestionHelper.createQuiz(3, name = "abcd", "right"))
        quizzes.add(QuestionHelper.createQuiz(4, name = "testons", "left"))

        getAllQuizzes.execute(
                "name",
                "test",
                isDescending = true
        )
        Mockito.verify(quizDao).findAll(Sort.by(Sort.Direction.DESC, "name"))
    }

    @Test
    fun `getAllQuizzes when isDescending is true and filter is rate should order quizzes by top rate to low`() {
        val quizzes = mutableListOf<Quiz>()
        quizzes.add(QuestionHelper.createQuiz(1, name = "test", "rights", rate = 3.2))
        quizzes.add(QuestionHelper.createQuiz(2, name = "tests", "left", rate = 2.6))
        quizzes.add(QuestionHelper.createQuiz(3, name = "abcd", "right", rate = 5.0))
        quizzes.add(QuestionHelper.createQuiz(4, name = "testons", "left", rate = 3.0))

        getAllQuizzes.execute(
                "rate",
                null,
                isDescending = true
        )

        Mockito.verify(quizDao, Mockito.times(1))
                .findAll(Sort.by(Sort.Direction.DESC, "rate"))
    }

    @Test
    fun `getAllQuizzes when filter value is not send should return all quizzes`() {
        val quizzes = mutableListOf<Quiz>()
        quizzes.add(QuestionHelper.createQuiz(1, name = "test", "description", lang = "fr"))
        quizzes.add(QuestionHelper.createQuiz(2, name = "tests", "description", lang = "fr"))
        quizzes.add(QuestionHelper.createQuiz(3, name = "test", "description", lang = "en"))
        quizzes.add(QuestionHelper.createQuiz(4, name = "abcd", "efgh"))
        quizzes.add(QuestionHelper.createQuiz(5, name = "testa", "description", lang = "fr"))

        getAllQuizzes.execute(
                "lang",
                null
        )

        Mockito.verify(quizDao, Mockito.times(1))
                .findAll(Sort.by(Sort.Direction.ASC, "lang"))
    }

    @Test
    fun `getAllQuizzes when request selected quiz should select all selected quizzes by user`() {
        val quizzes = mutableListOf<Quiz>()
        val currentUser = UserHelper.createUser(1, "toto@tata.fr", "totopassword", "totpseudo")
        quizzes.add(QuestionHelper.createQuiz(1, name = "test", "rights", users = setOf(currentUser)))
        quizzes.add(QuestionHelper.createQuiz(2, name = "tests", "left"))
        quizzes.add(QuestionHelper.createQuiz(3, name = "abcd", "right"))
        val expectedQuizzes = mutableListOf<Quiz>()
        expectedQuizzes.add(QuestionHelper.createQuiz(1, name = "test", "rights", users = setOf(currentUser)))
        Mockito.`when`(quizDao.findAll(Mockito.any(Sort::class.java))).thenReturn(quizzes)

        val result = getAllQuizzes.execute(
                null,
                null,
                isSelected = true,
                userId = 1
        )
        assertThat(result.size).isEqualTo(1)
        assertThat(result.containsAll(expectedQuizzes)).isTrue
        assertThat(expectedQuizzes.containsAll(result)).isTrue
    }

    @Test
    fun `getAllQuizzes when request deselected quiz should select all quizzes not selected by user`() {
        val quizzes = mutableListOf<Quiz>()
        val currentUser = UserHelper.createUser(1, "toto@tata.fr", "totopassword", "totpseudo")
        quizzes.add(QuestionHelper.createQuiz(1, name = "test", "rights", users = setOf(currentUser)))
        quizzes.add(QuestionHelper.createQuiz(2, name = "tests", "left"))
        quizzes.add(QuestionHelper.createQuiz(3, name = "abcd", "right"))
        val expectedQuizzes = mutableListOf<Quiz>()
        expectedQuizzes.add(QuestionHelper.createQuiz(2, name = "tests", "left"))
        expectedQuizzes.add(QuestionHelper.createQuiz(3, name = "abcd", "right"))
        Mockito.`when`(quizDao.findAll(Mockito.any(Sort::class.java))).thenReturn(quizzes)

        val result = getAllQuizzes.execute(
                null,
                null,
                isSelected = false,
                userId = 1
        )
        assertThat(result.size).isEqualTo(2)
        assertThat(result.containsAll(expectedQuizzes)).isTrue
        assertThat(expectedQuizzes.containsAll(result)).isTrue
    }

    @Test
    fun `getAllQuizzes when request selected and filer is name should selected all quizzes selected by user and filter by name`() {
        val quizzes = mutableListOf<Quiz>()
        val currentUser = UserHelper.createUser(1, "toto@tata.fr", "totopassword", "totpseudo")
        quizzes.add(QuestionHelper.createQuiz(1, name = "tests", "rights", users = setOf(currentUser)))
        quizzes.add(QuestionHelper.createQuiz(2, name = "test", "left"))
        quizzes.add(QuestionHelper.createQuiz(3, name = "abcd", "right"))
        quizzes.add(QuestionHelper.createQuiz(4, name = "test", "rights", users = setOf(currentUser)))
        val expectedQuizzes = mutableListOf<Quiz>()
        expectedQuizzes.add(QuestionHelper.createQuiz(1, name = "tests", "rights", users = setOf(currentUser)))
        expectedQuizzes.add(QuestionHelper.createQuiz(4, name = "test", "rights", users = setOf(currentUser)))
        Mockito.`when`(quizDao.findAll(Sort.by(Sort.Direction.ASC, "name"))).thenReturn(quizzes)

        val result = getAllQuizzes.execute(
                "name",
                "test",
                isSelected = true,
                userId = 1
        )
        assertThat(result.size).isEqualTo(2)
        assertThat(result.containsAll(expectedQuizzes)).isTrue
        assertThat(expectedQuizzes.containsAll(result)).isTrue
    }

    @Test
    fun `getAllQuizzes when request selected quizzes without user id should throw exception`() {
        assertThatThrownBy {
            getAllQuizzes.execute(null, null, isSelected = true, userId = null)
        }.isInstanceOf(Exception::class.java)
                .hasMessage("NullUserIdSelectedQuizzes")
    }
}