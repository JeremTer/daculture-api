package fr.esgi.daculture.service.lobby

import fr.esgi.daculture.dao.LobbyDao
import fr.esgi.daculture.dao.VoteDao
import fr.esgi.daculture.helper.AnswerHelper
import fr.esgi.daculture.helper.LobbyHelper
import fr.esgi.daculture.helper.UserHelper
import fr.esgi.daculture.helper.VoteHelper
import fr.esgi.daculture.model.Lobby
import fr.esgi.daculture.model.Quiz
import fr.esgi.daculture.model.Vote
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.boot.test.mock.mockito.MockBean
import java.util.*

class GetVotesOfLobbyTest {
    private lateinit var getVotesOfLobby: GetVotesOfLobby

    @MockBean
    lateinit var lobbyDao: LobbyDao

    @MockBean
    lateinit var voteDao: VoteDao

    private val lobbyId = 3L
    private val testLobby = LobbyHelper.createLobby(lobbyId, password = "creatorPassword", quiz = Quiz())

    @BeforeEach
    fun setup() {
        lobbyDao = Mockito.mock(LobbyDao::class.java)
        voteDao = Mockito.mock(VoteDao::class.java)
        getVotesOfLobby = GetVotesOfLobby(lobbyDao, voteDao)
        Mockito.`when`(lobbyDao.findById(lobbyId)).thenReturn(Optional.of(testLobby))
    }

    @Test
    fun `should throw exception when lobby id is null`() {
        assertThatThrownBy {
            getVotesOfLobby.execute(null)
        }.isInstanceOf(Exception::class.java)
                .hasMessage("NullLobbyId")
    }

    @Test
    fun `getVotesOfLobby should throw exception if lobby not exist`() {
        Mockito.`when`(lobbyDao.findById(lobbyId)).thenReturn(Optional.empty())
        assertThatThrownBy {
            getVotesOfLobby.execute(lobbyId)
        }.isInstanceOf(Exception::class.java)
                .hasMessage("NotLobbyExist")
    }

    @Test
    fun `should call vote dao to get votes by lobby id`() {
        getVotesOfLobby.execute(lobbyId)
        Mockito.verify(voteDao).findVotesByLobby(lobbyId)
    }

    @Test
    fun `should throw exception when find votes by a lobby return null`() {
        Mockito.`when`(voteDao.findVotesByLobby(lobbyId)).thenReturn(null)
        assertThatThrownBy {
            getVotesOfLobby.execute(lobbyId)
        }.isInstanceOf(Exception::class.java)
                .hasMessage("NullVotes")
    }


    @Test
    fun `should throw exception when one vote miss user information`() {
        val votes = mutableSetOf<Vote>()
        votes.add(
                VoteHelper.createVote(
                        1,
                        true,
                        UserHelper.createUser(1, "email@email.fr", "password", "pseudo"),
                        LobbyHelper.createLobby(2, password = "passwordLobby", quiz = Quiz()),
                        answer = AnswerHelper.createAnswer(1, "answer content", 1, mutableSetOf())
                )
        )
        votes.add(
                VoteHelper.createVote(
                        2,
                        true,
                        null,
                        LobbyHelper.createLobby(2, password = "passwordLobby", quiz = Quiz()),
                        answer = AnswerHelper.createAnswer(1, "answer content", 1, mutableSetOf())
                )
        )
        Mockito.`when`(voteDao.findVotesByLobby(lobbyId)).thenReturn(votes)
        assertThatThrownBy {
            getVotesOfLobby.execute(lobbyId)
        }.isInstanceOf(Exception::class.java)
                .hasMessage("AtLeastOneVoteNoUser")
    }

    @Test
    fun `should throw exception if one vote miss lobby information`() {
        val votes = mutableSetOf<Vote>()
        votes.add(
                VoteHelper.createVote(
                        1,
                        true,
                        UserHelper.createUser(1, "email@email.fr", "password", "pseudo"),
                        null,
                        answer = AnswerHelper.createAnswer(1, "answer content", 1, mutableSetOf())
                )
        )
        votes.add(
                VoteHelper.createVote(
                        2,
                        true,
                        UserHelper.createUser(2, "email2@email.fr", "password2", "pseudo2"),
                        LobbyHelper.createLobby(2, password = "passwordLobby", quiz = Quiz()),
                        answer = AnswerHelper.createAnswer(1, "answer content", 1, mutableSetOf())
                )
        )
        Mockito.`when`(voteDao.findVotesByLobby(lobbyId)).thenReturn(votes)
        assertThatThrownBy {
            getVotesOfLobby.execute(lobbyId)
        }.isInstanceOf(Exception::class.java)
                .hasMessage("AtLeastOneVoteNoLobby")
    }

    @Test
    fun `should throw exception if one vote miss answer information`() {
        val votes = mutableSetOf<Vote>()
        votes.add(
                VoteHelper.createVote(
                        1,
                        true,
                        UserHelper.createUser(1, "email@email.fr", "password", "pseudo"),
                        LobbyHelper.createLobby(2, password = "passwordLobby", quiz = Quiz()),
                        answer = null
                )
        )
        votes.add(
                VoteHelper.createVote(
                        2,
                        true,
                        UserHelper.createUser(2, "email2@email.fr", "password2", "pseudo2"),
                        LobbyHelper.createLobby(2, password = "passwordLobby", quiz = Quiz()),
                        answer = AnswerHelper.createAnswer(1, "answer content", null, mutableSetOf())
                )
        )
        Mockito.`when`(voteDao.findVotesByLobby(lobbyId)).thenReturn(votes)
        assertThatThrownBy {
            getVotesOfLobby.execute(lobbyId)
        }.isInstanceOf(Exception::class.java)
                .hasMessage("AtLeastOneVoteNoAnswer")
    }

    @Test
    fun `should throw exception if one answer not contain question`() {
        val votes = mutableSetOf<Vote>()
        votes.add(
                VoteHelper.createVote(
                        1,
                        true,
                        UserHelper.createUser(1, "email@email.fr", "password", "pseudo"),
                        LobbyHelper.createLobby(2, password = "passwordLobby", quiz = Quiz()),
                        answer = AnswerHelper.createAnswer(1, "answer content", 1, mutableSetOf())
                )
        )
        votes.add(
                VoteHelper.createVote(
                        2,
                        true,
                        UserHelper.createUser(2, "email2@email.fr", "password2", "pseudo2"),
                        LobbyHelper.createLobby(2, password = "passwordLobby", quiz = Quiz()),
                        answer = AnswerHelper.createAnswer(1, "answer content", null, mutableSetOf())
                )
        )
        Mockito.`when`(voteDao.findVotesByLobby(lobbyId)).thenReturn(votes)
        assertThatThrownBy {
            getVotesOfLobby.execute(lobbyId)
        }.isInstanceOf(Exception::class.java)
                .hasMessage("AtLeastOneAnswerNoQuestion")
    }

    @Test
    fun `should return set of votes`() {
        val votes = mutableSetOf<Vote>()
        votes.add(
                VoteHelper.createVote(
                        1,
                        true,
                        UserHelper.createUser(1, "email@email.fr", "password", "pseudo"),
                        LobbyHelper.createLobby(2, password = "passwordLobby", quiz = Quiz()),
                        answer = AnswerHelper.createAnswer(1, "answer content", 1, mutableSetOf())
                )
        )
        votes.add(
                VoteHelper.createVote(
                        2,
                        true,
                        UserHelper.createUser(2, "email2@email.fr", "password2", "pseudo2"),
                        LobbyHelper.createLobby(2, password = "passwordLobby", quiz = Quiz()),
                        answer = AnswerHelper.createAnswer(2, "answer content", 2, mutableSetOf())
                )
        )
        Mockito.`when`(voteDao.findVotesByLobby(lobbyId)).thenReturn(votes)

        val result = getVotesOfLobby.execute(lobbyId)
        Assertions.assertThat(result).isEqualTo(votes)
    }
}
