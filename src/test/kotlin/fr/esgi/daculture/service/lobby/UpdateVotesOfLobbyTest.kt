package fr.esgi.daculture.service.lobby

import fr.esgi.daculture.dao.LobbyDao
import fr.esgi.daculture.dao.VoteDao
import fr.esgi.daculture.helper.AnswerHelper
import fr.esgi.daculture.helper.LobbyHelper
import fr.esgi.daculture.helper.UserHelper
import fr.esgi.daculture.helper.VoteHelper
import fr.esgi.daculture.model.Lobby
import fr.esgi.daculture.model.Quiz
import fr.esgi.daculture.model.Vote
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.boot.test.mock.mockito.MockBean
import java.util.*

class UpdateVotesOfLobbyTest {
    private lateinit var updateVotesOfLobby: UpdateVotesOfLobby

    @MockBean
    lateinit var lobbyDao: LobbyDao

    @MockBean
    lateinit var voteDao: VoteDao

    private val lobbyId = 2L
    private lateinit var testLobby: Lobby
    private val testVotes = mutableListOf<Vote>()

    @BeforeEach
    fun setup() {
        lobbyDao = Mockito.mock(LobbyDao::class.java)
        voteDao = Mockito.mock(VoteDao::class.java)
        updateVotesOfLobby = UpdateVotesOfLobby(lobbyDao, voteDao)
        testLobby = LobbyHelper.createLobby(lobbyId, password = "updatePassword", quiz = Quiz())
        testVotes.add(
                VoteHelper.createVote(
                        1,
                        true,
                        UserHelper.createUser(1, "email@email.fr", "password", "pseudo"),
                        LobbyHelper.createLobby(2, password = "passwordLobby", quiz = Quiz()),
                        answer = AnswerHelper.createAnswer(1, "answer content", 1, mutableSetOf())
                )
        )
        testVotes.add(
                VoteHelper.createVote(
                        2,
                        true,
                        UserHelper.createUser(2, "email2@email.fr", "password2", "pseudo2"),
                        LobbyHelper.createLobby(2, password = "passwordLobby", quiz = Quiz()),
                        answer = AnswerHelper.createAnswer(2, "answer content", 2, mutableSetOf())
                )
        )
        Mockito.`when`(lobbyDao.findById(lobbyId)).thenReturn(Optional.of(testLobby))
        Mockito.`when`(voteDao.findAllById(Mockito.anyCollection())).thenReturn(testVotes)
    }

    @Test
    fun `should throw exception when the lobby id is null`() {
        assertThatThrownBy {
            updateVotesOfLobby.execute(null, testVotes)
        }.isInstanceOf(Exception::class.java)
                .hasMessage("NullLobbyId")
    }

    @Test
    fun `should throw exception when votes is null`() {
        assertThatThrownBy {
            updateVotesOfLobby.execute(lobbyId, null)
        }.isInstanceOf(Exception::class.java)
                .hasMessage("NullVotes")
    }

    @Test
    fun `should call the the lobby dao to get lobby by id`() {
        updateVotesOfLobby.execute(lobbyId, testVotes)
        Mockito.verify(lobbyDao).findById(lobbyId)
    }

    @Test
    fun `should throw exception when lobby not found`() {
        Mockito.`when`(lobbyDao.findById(lobbyId)).thenReturn(Optional.empty())
        assertThatThrownBy {
            updateVotesOfLobby.execute(lobbyId, testVotes)
        }.isInstanceOf(Exception::class.java)
                .hasMessage("NotLobbyExist")
    }

    @Test
    fun `should call vote dao to find all votes with param votes ids`() {
        val expectedIds = testVotes.map { v -> v.id }

        updateVotesOfLobby.execute(lobbyId, testVotes)
        Mockito.verify(voteDao).findAllById(expectedIds)
    }

    @Test
    fun `should throw exception if find all votes by ids return not same length than param votes`() {
        val returnVotes = mutableListOf<Vote>()
        returnVotes.add(
                VoteHelper.createVote(
                        3,
                        true,
                        UserHelper.createUser(1, "email@email.fr", "password", "pseudo"),
                        LobbyHelper.createLobby(2, password = "passwordLobby", quiz = Quiz()),
                        answer = AnswerHelper.createAnswer(1, "answer content", 1, mutableSetOf())
                )
        )
        testVotes.map { v -> v.id }
        Mockito.`when`(voteDao.findAllById(Mockito.anyCollection())).thenReturn(returnVotes)

        assertThatThrownBy {
            updateVotesOfLobby.execute(lobbyId, testVotes)
        }.isInstanceOf(Exception::class.java)
                .hasMessage("NotCorrectGivenVotes")
    }

    @Test
    fun `should save new votes`() {
        testLobby.isClosed = true

        assertThatThrownBy {
            updateVotesOfLobby.execute(lobbyId, testVotes)
        }.isInstanceOf(Exception::class.java)
                .hasMessage("LobbyAlreadyClosed")
    }
}